<?php

namespace Infotechnohelp\Bakery\Bin;

use Infotechnohelp\Bakery\Containers\Bakery\ContainerFileContainer;

require dirname(__DIR__ ). '/vendor/autoload.php';


$fileTitle = $argv[1];

$ds = DIRECTORY_SEPARATOR;

$filePath = dirname(__DIR__) . $ds . 'src' . $ds . 'Containers' . $ds . $fileTitle . 'Container.php';

$result = (new ContainerFileContainer())
    ->setInput([
        'fileTitle' => $fileTitle
    ])
    ->render();

file_put_contents($filePath, $result);