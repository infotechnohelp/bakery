<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Init;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Containers\CakePhp\BasicEntityFileContainer;
use Infotechnohelp\Bakery\Containers\CakePhp\BasicMigrationFileContainer;
use Infotechnohelp\Bakery\Containers\CakePhp\BasicModelManagerContainer;
use Infotechnohelp\Bakery\Containers\CakePhp\BasicSeedsFileContainer;
use Infotechnohelp\Bakery\Containers\CakePhp\BasicSeedsQueueFileContainer;
use Infotechnohelp\Bakery\Containers\CakePhp\BasicTableFileContainer;
use Infotechnohelp\Bakery\Lib\Parser\CakePhpModelSchema;
use Symfony\Component\Yaml\Yaml;

class CakePhp
{
    /**
     * @param array $paths
     * @param string $outputPath
     */
    private static function saveYaml(array $paths, string $type)
    {
        $result = "";

        foreach ($paths as $path) {
            $result .= "- $path\n";
        }


        $result .= "- config\Bakery\CakePhpBakedFiles\\$type.yml\n";

        switch ($type) {
            case "model":
                $result .= "- config\Migrations\schema-dump-default.lock";
                break;
        }

        $outputPath = "config/Bakery/CakePhpBakedFiles/$type.yml";

        file_put_contents($outputPath, $result);
    }

    /**
     * @param string $configPath
     */
    public static function model(string $configPath)
    {
        $files = [];

        $result = (new BasicMigrationFileContainer())
            ->setInput([
                'classTitle' => 'Initialize'
            ])
            ->setYamlConfigPath($configPath)
            ->render();

        $timestamp = date("YmdHis");

        $migrationFilePath = "config/Migrations/{$timestamp}_Initialize.php";

        file_put_contents($migrationFilePath, $result);

        $files[] = $migrationFilePath;

        $tableConfigs = CakePhpModelSchema::getTableConfigs($configPath);

        foreach ($tableConfigs as $tableConfig) {

            $result = (new BasicTableFileContainer())
                ->setInput($tableConfig)
                ->render();

            file_put_contents("src/Model/Table/{$tableConfig['tableTitle']}Table.php", $result);

            $files[] = "src/Model/Table/{$tableConfig['tableTitle']}Table.php";
        }

        $parsedEntityYaml = CakePhpModelSchema::extractEntitySchema(
            Yaml::parse(
                file_get_contents($configPath)
            )
        );

        $entityConfigs = CakePhpModelSchema::getEntityConfigs($tableConfigs, $parsedEntityYaml);

        foreach ($entityConfigs as $entityConfig) {

            $result = (new BasicEntityFileContainer())
                ->setInput($entityConfig)
                ->render();

            file_put_contents("src/Model/Entity/{$entityConfig['fileTitle']}.php", $result);

            $files[] = "src/Model/Entity/{$entityConfig['fileTitle']}.php";

            if (array_key_exists('data', $entityConfig) && !empty($entityConfig['data'])) {

                $result = (new BasicSeedsFileContainer())
                    ->setInput([
                        'fileTitle' => $entityConfig['fileTitle'],
                        'data' => $entityConfig['data'],
                    ])
                    ->render();

                $tableTitle = Inflector::pluralize($entityConfig['fileTitle']);

                file_put_contents("config/TrackedSeeds/$tableTitle.php", $result);

                $files[] = "config/TrackedSeeds/$tableTitle.php";
            }

        }

        $result = (new BasicSeedsQueueFileContainer())
            ->setInput($entityConfigs)
            ->render();

        file_put_contents("config/TrackedSeeds/_Queue.php", $result);

        $files[] = "config/TrackedSeeds/_Queue.php";

        self::saveYaml($files, 'model');
    }

    private static function getTableConfigByFileTitle(string $fileTitle, array $tableConfigs)
    {
        foreach ($tableConfigs as $tableConfig) {
            if ($tableConfig['tableTitle'] === $fileTitle) {
                return $tableConfig;
            }
        }

        return null;
    }

    /**
     * @param string $modelConfigPath
     */
    public static function modelManager(string $modelConfigPath, string $modelManagerConfigPath = null)
    {
        $mainTableTitles = CakePhpModelSchema::getMainTableTitles($modelConfigPath);

        if ($modelManagerConfigPath !== null) {
            $modelManagerConfig = Yaml::parse(file_get_contents($modelManagerConfigPath));
        }

        if (isset($modelManagerConfig['exclude'])) {
            $input = [];

            foreach ($mainTableTitles as $tableTitle){
                if(!in_array($tableTitle, $modelManagerConfig['exclude'], true)){
                    $input[] = $tableTitle;
                }
            }
        }

        if (isset($modelManagerConfig['include'])) {
            $input = $modelManagerConfig['include'];
        }

        if (!isset($input)) {
            $input = $mainTableTitles;
        }


        $files = [];

        $tableConfigs = CakePhpModelSchema::getTableConfigs($modelConfigPath);

        foreach ($input as $managertTitle) {

            $tableConfig = self::getTableConfigByFileTitle($managertTitle, $tableConfigs);

            $modelManagerConfig = CakePhpModelSchema::getModelManagerConfigs($tableConfig);

            $result = (new BasicModelManagerContainer())
                ->setInput($modelManagerConfig)
                ->render();

            file_put_contents("src/ModelManager/{$tableConfig['tableTitle']}.php", $result);

            $files[] = "src/ModelManager/{$tableConfig['tableTitle']}.php";
        }

        self::saveYaml($files, 'model-manager');
    }
}