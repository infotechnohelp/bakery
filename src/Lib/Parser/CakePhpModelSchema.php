<?php

namespace Infotechnohelp\Bakery\Lib\Parser;

use Cake\Utility\Inflector;
use Symfony\Component\Yaml\Yaml;

class CakePhpModelSchema
{
    const ADDING_SEPARATOR = '+';
    const LINKING_COLUMN_SEPARATOR = '.';
    const LINKING_TABLE_SEPARATOR = '..';
    const PLUGIN_PREFIX_SEPARATOR = ':';
    const NULLABLE_LABEL = '{null}';
    const COLUMN_TYPE_SEPARATOR = ':';
    const CUSTOM_LINKING_COLUMN_REGEX = '(\([a-zA-z,]{4,}\))';

    public static function getMainTableTitles($yamlMigrationConfigPath)
    {
        $result = [];

        foreach (Yaml::parse(file_get_contents($yamlMigrationConfigPath)) as $alias => $config) {
            if (self::identifyTableAlias($alias) === 'default') {
                $result[] = self::parseTableAlias($alias)['tableTitle'];
            }
        }

        return $result;
    }

    public static function getEntityConfigs(array $tableConfigs, array $entityConfigs = []): array
    {
        $result = [];

        foreach ($tableConfigs as $tableConfig) {

            $config = [
                'fileTitle' => Inflector::singularize($tableConfig['tableTitle']),
            ];

            if (isset($entityConfigs[$tableConfig['tableTitle']])) {

                if (isset($entityConfigs[$tableConfig['tableTitle']]['data'])) {
                    $config['data'] = $entityConfigs[$tableConfig['tableTitle']]['data'];
                }

                if (isset($entityConfigs[$tableConfig['tableTitle']]['virtual'])) {
                    $config['virtual'] = $entityConfigs[$tableConfig['tableTitle']]['virtual'];
                }
            }

            if (isset($tableConfig['timestampBehaviour']) && $tableConfig['timestampBehaviour']) {
                $config['timestampBehaviour'] = true;
            }

            $columns = [];

            if (isset($tableConfig['add-relations'])) {
                foreach ($tableConfig['add-relations'] as $relationTitle => $table) {

                    $tableTitle = array_keys($table)[0];

                    if (in_array($relationTitle, ['belongsTo', 'hasOne'], true)) {
                        $columns[] = Inflector::underscore(Inflector::singularize($tableTitle));
                    }

                    if (in_array($relationTitle, ['belongsToMany', 'hasMany'], true)) {
                        $columns[] = Inflector::underscore($tableTitle);
                    }
                }
            }

            if (isset($tableConfig['validations'])) {
                foreach ($tableConfig['validations'] as $columnTitle => $validationConfig) {
                    $columns[] = $columnTitle;
                }
            }


            if (isset($tableConfig['relations']['hasOne'])) {
                foreach ($tableConfig['relations']['hasOne'] as $tableTitle => $hasOneTableConfig) {
                    $columns[] = Inflector::underscore(Inflector::singularize($tableTitle));
                }
            }

            if (isset($tableConfig['relations']['hasMany'])) {
                foreach ($tableConfig['relations']['hasMany'] as $tableTitle => $hasManyTableConfig) {
                    $columns[] = Inflector::underscore($tableTitle);
                }
            }

            if (isset($tableConfig['relations']['belongsTo'])) {
                foreach ($tableConfig['relations']['belongsTo'] as $tableTitle => $belongsToTableConfig) {
                    //$columns[] = Inflector::underscore(Inflector::singularize($tableTitle)) . "_id";

                    if (isset($belongsToTableConfig['propertyName'])) {
                        $columns[] = $belongsToTableConfig['propertyName'];
                        continue;
                    }

                    $columns[] = Inflector::underscore(Inflector::singularize($tableTitle));
                }
            }

            if (isset($tableConfig['relations']['belongsToMany'])) {
                foreach ($tableConfig['relations']['belongsToMany'] as $tableTitle => $belongsToManyTableConfig) {
                    $columns[] = Inflector::underscore($tableTitle);
                }
            }

            $config['columns'] = $columns;

            $result[] = $config;
        }

        /*
                 'fileTitle' => 'Article',
        'timestampBehaviour' => true,
        'fields' => [
            'user_id', 'user', 'text', 'tags',
        ],
         */
        return $result;
    }

    /**
     * @param $yamlMigrationConfigPath
     * @param null $yamlTableConfigPath
     * @return array
     */
    public static function getTableConfigs(string $yamlMigrationConfigPath): array
    {
        return self::prepareTableConfigs(
            Yaml::parse(file_get_contents($yamlMigrationConfigPath))
        );
    }


    private static function trimTableConfigAlias(string $alias)
    {
        if (strpos($alias, 't_') === 0) {
            return str_replace('t_', '', $alias);
        }

        if (strpos($alias, 'table_') === 0) {
            return str_replace('table_', '', $alias);
        }

        return $alias;
    }

    private static function trimEntityConfigAlias(string $alias)
    {
        if (strpos($alias, 'e_') === 0) {
            return str_replace('e_', '', $alias);
        }

        if (strpos($alias, 'entity_') === 0) {
            return str_replace('entity_', '', $alias);
        }

        return $alias;
    }

    private static function identifyTableAlias(string $alias)
    {
        if (strpos($alias, 't_') === 0 || strpos($alias, 'table_') === 0) {
            return 'table';
        }

        if (strpos($alias, 'e_') === 0 || strpos($alias, 'entity_') === 0) {
            return 'entity';
        }

        if (strpos($alias, 'db_') === 0 || strpos($alias, 'database_') === 0) {
            return 'database';
        }

        return 'default';
    }

    private static function extractDbSchema(array $parsedYamlConfig)
    {
        $result = [];

        foreach ($parsedYamlConfig as $alias => $config) {
            if (self::identifyTableAlias($alias) === 'default' || self::identifyTableAlias($alias) === 'database') {
                $result[$alias] = $config;
            }
        }

        return $result;
    }

    private static function extractTableSchema(array $parsedYamlConfig)
    {
        $result = [];

        foreach ($parsedYamlConfig as $alias => $config) {
            if (self::identifyTableAlias($alias) === 'table') {
                $result[self::trimTableConfigAlias($alias)] = $config;
            }
        }

        return $result;
    }

    public static function extractEntitySchema(array $parsedYamlConfig)
    {
        $result = [];

        foreach ($parsedYamlConfig as $alias => $config) {
            if (self::identifyTableAlias($alias) === 'entity') {
                $result[self::trimEntityConfigAlias($alias)] = $config;
            }
        }

        return $result;
    }

    private static function prepareTableConfigs(array $parsedYamlConfig): array
    {

        $result = [];

        $tableIndexes = [];

        $linkingTables = [];

        $parsedMigrationYaml = self::extractDbSchema($parsedYamlConfig);
        $parsedModelYaml = self::extractTableSchema($parsedYamlConfig);

        foreach ($parsedMigrationYaml as $tableAlias => $columns) {

            if (self::identifyTableAlias($tableAlias) === 'database') {

                $index = $tableIndexes[self::trimDatabaseConfigAlias($tableAlias)];

                $result[$index]['additional'] = $columns;

                // BUG, obsolete section
/*
                foreach ($columns['uniqueFieldGroups'] as $fields) {
                    foreach ($fields as $field) {
                        $result[$index]['validations'][$field]['isUnique'] = null;
                    }
                }
*/

                continue;
            }

            $parsedTableAlias = self::parseTableAlias($tableAlias);
            $result[]['tableTitle'] = $parsedTableAlias['tableTitle'];

            $index = count($result) - 1;


            if (isset($parsedModelYaml[$parsedTableAlias['tableTitle']]['add-relations'])) {
                $result[$index]['add-relations'] = $parsedModelYaml[$parsedTableAlias['tableTitle']]['add-relations'];
            }


            $tableIndexes[$parsedTableAlias['tableTitle']] = $index;


            foreach ($parsedTableAlias['linking']['columns'] as $tableTitle => $linkedTableConfig) {

                $result[$index]['relations']['belongsTo'][$tableTitle] = $linkedTableConfig;

                $result[$index]['buildRules']['existsIn'][$tableTitle] = $linkedTableConfig;


                $config = [
                    'integer' => null,
                    'allowEmptyString' => null,
                ];

                // This section was used to require linking fields, but it is obsolete
                /*
                if (array_key_exists('nullable', $linkedTableConfig) && $linkedTableConfig['nullable']) {
                    $config['allowEmptyString'] = null;
                } else {
                    $config['requirePresence'] = null;
                    $config['allowEmptyString'] = [false];
                }
                */

                if (
                    !array_key_exists('nullable', $linkedTableConfig) ||
                    array_key_exists('nullable', $linkedTableConfig) && !$linkedTableConfig['nullable']
                ) {

                    $propertyName = Inflector::singularize(Inflector::underscore($tableTitle));

                    $foreignKey = "{$propertyName}_id";


                    if (isset($linkedTableConfig['propertyName'])) {
                        $propertyName = $linkedTableConfig['propertyName'];
                    }

                    if (isset($linkedTableConfig['customLinkingColumn'])) {
                        $foreignKey = $linkedTableConfig['customLinkingColumn'];
                    }

                    $result[$index]['buildRules']['requireAtLeastOneOf'][] = [
                        $propertyName, $foreignKey,
                    ];

                }

                $columnTitle = Inflector::singularize(Inflector::underscore($tableTitle)) . "_id";

                if (isset($linkedTableConfig['customLinkingColumn'])) {
                    $columnTitle = $linkedTableConfig['customLinkingColumn'];
                }

                $result[$index]['validations'][$columnTitle] = $config;
            }


            // HANDLING CUSTOM RULES HERE

            if (isset($parsedModelYaml[$parsedTableAlias['tableTitle']]['rules']['requireAtLeastOneOf'])) {
                $result[$index]['buildRules']['requireAtLeastOneOf'][] =
                    $parsedModelYaml[$parsedTableAlias['tableTitle']]['rules']['requireAtLeastOneOf'];
            }

            if (isset($parsedModelYaml[$parsedTableAlias['tableTitle']]['rules']['requireAtLeastOneOf'])) {
                $result[$index]['buildRules']['requireAtLeastOneOfGroups'][] =
                    $parsedModelYaml[$parsedTableAlias['tableTitle']]['rules']['requireAtLeastOneOfGroups'];
            }

            if (isset($parsedModelYaml[$parsedTableAlias['tableTitle']]['rules']['requireAtLeastOneOf'])) {
                $result[$index]['buildRules']['requireOnlyOneOf'][] =
                    $parsedModelYaml[$parsedTableAlias['tableTitle']]['rules']['requireOnlyOneOf'];
            }

            if (isset($parsedModelYaml[$parsedTableAlias['tableTitle']]['rules']['requireAtLeastOneOf'])) {
                $result[$index]['buildRules']['requireOnlyOneOfGroups'][] =
                    $parsedModelYaml[$parsedTableAlias['tableTitle']]['rules']['requireOnlyOneOfGroups'];
            }


            foreach ($parsedTableAlias['linking']['tables'] as $tableTitle => $linkedTableConfig) {

                $columnTitle = Inflector::underscore($tableTitle);

                $result[$index]['validations'][$columnTitle]['IdsExistIn'] = null;

                if (
                    !array_key_exists('nullable', $linkedTableConfig) ||
                    array_key_exists('nullable', $linkedTableConfig) && !$linkedTableConfig['nullable']
                ) {

                    $result[$index]['validations'][$columnTitle]['requirePresence'] = ['create'];
                    $result[$index]['buildRules']['NotEmpty'][] = $columnTitle;
                }

                $result[$index]['relations']['belongsToMany'][$tableTitle] = $linkedTableConfig;

                $tables = [$parsedTableAlias['tableTitle'], $tableTitle];
                sort($tables);

                $linkingTables[] = implode('', $tables);

                $result[]['tableTitle'] = implode('', $tables);

                $index2 = count($result) - 1;

                $tableIndexes[implode('', $tables)] = $index2;

                $tablesContainer = [];

                foreach ($tables as $table) {
                    $tablesContainer[$table] = [];
                }

                $tablesContainer[$tableTitle] = $linkedTableConfig;

                $result[$index2]['relations']['belongsTo'] = $tablesContainer;

                $result[$index2]['buildRules']['existsIn'] = $tablesContainer;


                $columnTitle = Inflector::underscore(Inflector::singularize($tableTitle)) . "_id";

                $result[$index2]['validations'][$columnTitle] = [
                    'integer' => null,
                    'allowEmptyString' => [false],
                ];

                $columnTitle = Inflector::underscore(Inflector::singularize($parsedTableAlias['tableTitle'])) . "_id";

                $result[$index2]['validations'][$columnTitle] = [
                    'integer' => null,
                    'allowEmptyString' => [false],
                ];
            }

            $timestampBehaviour = true;

            if (!empty($columns)) {
                foreach ($columns as $columnAlias => $columnOptions) {

                    list($columnTitle, $columnType) = array_pad(explode(self::COLUMN_TYPE_SEPARATOR, $columnAlias), 2, null);

                    if ($columnTitle !== 'timestampBehaviour') {

                        $columnType = self::prepareType($columnType);
                        $columnOptions = self::prepareColumnOptions($columnType, $columnOptions);

                        if (in_array($columnType, ['boolean'], true)) {
                            $result[$index]['validations'][$columnTitle]['boolean'] = null;
                        }

                        if (in_array($columnType, ['datetime'], true)) {
                            $result[$index]['validations'][$columnTitle]['dateTime'] = null;
                        }

                        if (in_array($columnType, ['float'], true)) {
                            $result[$index]['validations'][$columnTitle]['numeric'] = null;
                        }

                        if (in_array($columnType, ['string', 'text'], true)) {
                            $result[$index]['validations'][$columnTitle]['scalar'] = null;
                        }

                        if (in_array($columnType, ['integer'], true)) {
                            $result[$index]['validations'][$columnTitle]['integer'] = null;
                        }

                        if (array_key_exists('unique', $columnOptions)) {
                            $result[$index]['buildRules']['isUnique'][] = $columnTitle;
                            $result[$index]['validations'][$columnTitle]['isUnique'] = null;
                        }

                        // @todo Test with integer
                        $minValue = ($columnType === 'float' && !$columnOptions['signed']) ? 0 : null;

                        if ($minValue !== null) {
                            $result[$index]['validations'][$columnTitle]['greaterThanOrEqual'] = [$minValue];
                        }

                        $limit = ($columnType === 'string') ? $columnOptions['limit'] : null;

                        if (!empty($limit)) {
                            $result[$index]['validations'][$columnTitle]['maxLength'] = [$limit];
                        }


                        $required = (!$columnOptions['null']) ? true : false;

                        if (array_key_exists('default', $columnOptions) && $columnOptions['default'] !== null) {
                            $required = false;
                        }

                        if ($required) {
                            $result[$index]['validations'][$columnTitle]['requirePresence'] = ['create'];
                            $result[$index]['validations'][$columnTitle]['allowEmptyString'] = [false];
                        } else {
                            $result[$index]['validations'][$columnTitle]['allowEmptyString'] = null;
                        }


                        // ADDITIONAL VALIDATIONS
                        if (isset($parsedModelYaml[$parsedTableAlias['tableTitle']]['validations'][$columnTitle])) {
                            foreach ($parsedModelYaml[$parsedTableAlias['tableTitle']]['validations'][$columnTitle] as
                                     $validationTitle => $validationConfig) {
                                $result[$index]['validations'][$columnTitle][$validationTitle] = $validationConfig;
                            }
                        }


                        // @todo Implement special validations later
                        /*
                        $result[$index]['validations'][$columnTitle] = [
                            $columnType, $required, $limit, $minValue,
                        ];
                        */

                    }

                    if ($columnTitle === 'timestampBehaviour' && !$columnOptions) {
                        $timestampBehaviour = false;
                    }
                }
            }

            if ($timestampBehaviour) {
                $result[$index]['timestampBehaviour'] = true;
            }

        }


        // SECOND ROUND TO SEARCH FOR hasMany or hasOne, belongsToMany

        foreach ($result as $tableConfig) {

            if (in_array($tableConfig['tableTitle'], $linkingTables, true)) {

                $tableIndex = $tableIndexes[$tableConfig['tableTitle']];

                $result[$tableIndex]['timestampBehaviour'] = true;

                continue;
            }


            $belongsToMany = $tableConfig['relations']['belongsToMany'] ?? [];

            foreach ($belongsToMany as $tableTitle => $belongsToManyTableConfig) {

                if (array_key_exists('pluginPrefix', $belongsToManyTableConfig)) {
                    continue;
                }


                $tableIndex = $tableIndexes[$tableTitle];

                $result[$tableIndex]['relations']['belongsToMany'][$tableConfig['tableTitle']] = [];
            }

            $belongsTo = $tableConfig['relations']['belongsTo'] ?? [];

            foreach ($belongsTo as $tableTitle => $belongsToTableConfig) {

                if (array_key_exists('pluginPrefix', $belongsToTableConfig)) {
                    continue;
                }

                $tableIndex = $tableIndexes[$tableTitle];

                $relationType = 'hasMany';


                if (isset($parsedModelYaml[$tableTitle]['rewrite-relations']['hasOne'])) {
                    if (in_array($tableConfig['tableTitle'], $parsedModelYaml[$tableTitle]['rewrite-relations']['hasOne'], true)) {
                        $relationType = 'hasOne';
                    }
                }

                $config = [];

                if (isset($tableConfig['relations']['belongsTo'][$tableTitle]['customLinkingColumn'])) {
                    $config['foreignKey'] = $tableConfig['relations']['belongsTo'][$tableTitle]['customLinkingColumn'];
                }


                $result[$tableIndex]['relations'][$relationType][$tableConfig['tableTitle']] = $config;
            }
        }

        return $result;
    }

    /**
     * @param $yamlConfigPath
     * @return array
     */
    public static function getMigrationConfig($yamlConfigPath): array
    {
        return self::prepareMigrationConfig(Yaml::parse(file_get_contents($yamlConfigPath)));
    }


    private static function trimDatabaseConfigAlias(string $alias)
    {
        if (strpos($alias, 'db_') === 0) {
            return str_replace('db_', '', $alias);
        }

        if (strpos($alias, 'database_') === 0) {
            return str_replace('database_', '', $alias);
        }

        return $alias;
    }

    /**
     * @param array $parsedYamlConfig
     * @return array
     */
    private static function prepareMigrationConfig(array $parsedYamlConfig): array
    {
        $result = [];

        $tableIndexes = [];

        $parsedMigrationYaml = self::extractDbSchema($parsedYamlConfig);


        foreach ($parsedMigrationYaml as $tableAlias => $columns) {


            if (self::identifyTableAlias($tableAlias) === 'database') {

                if (array_key_exists(self::trimDatabaseConfigAlias($tableAlias), $tableIndexes)) {

                    $index = $tableIndexes[self::trimDatabaseConfigAlias($tableAlias)];
                    $result[$index] = array_merge($result[$index], $columns);

                }

                continue;
            }


            $tableTitle = self::parseTableAlias($tableAlias)['tableTitle'];

            $tableIndexes[$tableTitle] = count($result);

            $tableResult = [
                'table' => self::parseTableAlias($tableAlias, $parsedMigrationYaml),
                'columns' => [],
            ];

            if ($columns !== null) {
                foreach ($columns as $columnsAlias => $columnOptions) {

                    $columnTitle = $columnsAlias;
                    $columnType = null;

                    if (strpos($columnsAlias, self::COLUMN_TYPE_SEPARATOR) !== false) {
                        list($columnTitle, $columnType) = explode(self::COLUMN_TYPE_SEPARATOR, $columnsAlias);
                    }

                    $tableResult['columns'][] = [
                        'title' => $columnTitle,
                        'type' => $columnType,
                        'options' => $columnOptions,
                    ];
                }
            }

            $result[] = $tableResult;
        }

        return $result;
    }


    /**
     * @param string $tableAlias
     * @return array
     */
    private static function parseTableAlias(string $tableAlias, array $migrationConfig = [])
    {
        $result = [
            'tableTitle' => null,
            'linking' => [
                'columns' => [],
                'tables' => [],
            ],
        ];

        $linkedTables = [];


        if (strpos($tableAlias, self::LINKING_TABLE_SEPARATOR) !== false) {
            $exploded = explode(self::LINKING_TABLE_SEPARATOR, $tableAlias);

            foreach (explode(self::ADDING_SEPARATOR, $exploded[0]) as $linkedTableAlias) {

                $linkedTableTitle = $linkedTableAlias;

                $config = [];

                if (strpos($linkedTableAlias, self::PLUGIN_PREFIX_SEPARATOR) !== false) {
                    $linkedTableTitle = explode(self::PLUGIN_PREFIX_SEPARATOR, $linkedTableAlias)[1];
                    $config['pluginPrefix'] = explode(self::PLUGIN_PREFIX_SEPARATOR, $linkedTableAlias)[0];
                }

                if (strpos($linkedTableAlias, self::NULLABLE_LABEL) !== false) {
                    $config['nullable'] = true;
                    $linkedTableTitle = str_replace(self::NULLABLE_LABEL, '', $linkedTableTitle);
                }

                $result['linking']['tables'][$linkedTableTitle] = $config;

                $linkedTables[] = $linkedTableTitle;
            }

            if (strpos($exploded[1], self::LINKING_COLUMN_SEPARATOR) === false) {
                $result['tableTitle'] = $exploded[1];
                return $result;
            }

            $exploded = explode(self::LINKING_COLUMN_SEPARATOR, $exploded[1]);

            foreach (explode(self::ADDING_SEPARATOR, $exploded[0]) as $linkedTableAlias) {

                $linkedTableTitle = $linkedTableAlias;

                $config = [];

                if (strpos($linkedTableAlias, self::PLUGIN_PREFIX_SEPARATOR) !== false) {
                    $config['pluginPrefix'] = explode(self::PLUGIN_PREFIX_SEPARATOR, $linkedTableAlias)[0];
                    $linkedTableTitle = explode(self::PLUGIN_PREFIX_SEPARATOR, $linkedTableAlias)[1];
                }

                if (strpos($linkedTableAlias, self::NULLABLE_LABEL) !== false) {
                    $config['nullable'] = true;
                    $linkedTableTitle = str_replace(self::NULLABLE_LABEL, '', $linkedTableTitle);
                }

                if (preg_match(self::CUSTOM_LINKING_COLUMN_REGEX, $linkedTableAlias, $matches)) {

                    $match = $matches[0];

                    $linkedTableTitle = str_replace($match, '', $linkedTableTitle);

                    $match = str_replace(['(', ')'], '', $match);

                    if (strpos($match, ",") !== false) {
                        list($foreignKey, $propertyName) = explode(",", $match);

                        if (!empty($foreignKey)) {
                            $config['customLinkingColumn'] = $foreignKey;
                        }

                        $config['propertyName'] = $propertyName;

                    } else {
                        $config['customLinkingColumn'] = $match;
                    }

                }


                $result['linking']['columns'][$linkedTableTitle] = $config;
            }


            $result['tableTitle'] = $exploded[1];


            foreach ($linkedTables as $linkedTable) {
                $tables = [$linkedTable, $exploded[1]];
                sort($tables);

                $title = implode('', $tables);

                if (array_key_exists('db_' . $title, $migrationConfig)) {
                    $uniqueFieldGroups = $migrationConfig['db_' . $title]['uniqueFieldGroups'];
                }

                if (array_key_exists('database_' . $title, $migrationConfig)) {
                    $uniqueFieldGroups = $migrationConfig['database_' . $title]['uniqueFieldGroups'];
                }

                if (isset($uniqueFieldGroups)) {
                    $result['linking']['tables'][$linkedTable]['uniqueFieldGroups'] = $uniqueFieldGroups;
                }
            }


            return $result;
        }

        if (strpos($tableAlias, self::LINKING_COLUMN_SEPARATOR) !== false) {
            $exploded = explode(self::LINKING_COLUMN_SEPARATOR, $tableAlias);

            foreach (explode(self::ADDING_SEPARATOR, $exploded[0]) as $linkedTableAlias) {

                $linkedTableTitle = $linkedTableAlias;

                $config = [];

                if (strpos($linkedTableAlias, self::PLUGIN_PREFIX_SEPARATOR) !== false) {
                    $config['pluginPrefix'] = explode(self::PLUGIN_PREFIX_SEPARATOR, $linkedTableAlias)[0];
                    $linkedTableTitle = explode(self::PLUGIN_PREFIX_SEPARATOR, $linkedTableAlias)[1];
                }

                if (strpos($linkedTableAlias, self::NULLABLE_LABEL) !== false) {
                    $config['nullable'] = true;
                    $linkedTableTitle = str_replace(self::NULLABLE_LABEL, '', $linkedTableTitle);
                }

                $result['linking']['columns'][$linkedTableTitle] = $config;
            }

            $result['tableTitle'] = $exploded[1];

            return $result;
        }

        $result['tableTitle'] = $tableAlias;

        return $result;
    }

    private static function updateYamlArray(array $firstArray, array $secondArray = null): array
    {
        if ($secondArray !== null) {
            foreach ($secondArray as $key => $value) {
                if ($key === '_null') {
                    $key = 'null';
                }

                $firstArray[$key] = $value;
            }
        }

        return $firstArray;
    }

    public static function prepareColumnOptions(string $type, array $columnOptions = null)
    {
        $ds = DIRECTORY_SEPARATOR;

        $configDirPath = dirname(dirname(dirname(__DIR__))) . $ds . 'config' . $ds;

        $defaultOptions =
            Yaml::parse(file_get_contents($configDirPath .
                "settings{$ds}Migrations{$ds}columnTypeDefaults$ds$type.yml"));


        $appConfigDirPath = dirname(dirname(dirname(dirname(dirname(dirname(__DIR__)))))) . $ds . 'config' . $ds;

        if (file_exists($appConfigDirPath . "Bakery{$ds}settings{$ds}Migrations{$ds}columnTypeDefaults$ds$type.yml")) {
            $appDefaultOptions =
                Yaml::parse(file_get_contents($appConfigDirPath .
                    "Bakery{$ds}settings{$ds}Migrations{$ds}columnTypeDefaults$ds$type.yml"));
        }

        if (isset($appDefaultOptions)) {
            foreach ($appDefaultOptions as $key => $value) {
                $defaultOptions[$key] = $value;
            }
        }


        if (array_key_exists('_null', $defaultOptions)) {
            $defaultOptions['null'] = $defaultOptions['_null'];
            unset($defaultOptions['_null']);
        }

        return self::updateYamlArray($defaultOptions, $columnOptions);
    }

    /**
     * @param string $type
     * @return mixed
     */
    public static function prepareType(string $type)
    {
        $configDirPath = dirname(dirname(dirname(dirname(dirname(dirname(__DIR__)))))) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR;

        if (file_exists($configDirPath . 'Bakery/settings/Migrations/columnTypeMap.yml')) {
            $map = Yaml::parse(file_get_contents($configDirPath . 'Bakery/settings/Migrations/columnTypeMap.yml'));

            if (array_key_exists($type, $map)) {
                return $map[$type];
            }
        }

        $defaultConfigDirPath = dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR;

        $defaultMap = Yaml::parse(file_get_contents($defaultConfigDirPath . 'settings/Migrations/columnTypeMap.yml'));

        return $defaultMap[$type];
    }

    public static function getModelManagerConfigs(array $tableConfig)
    {
        $result = [
            'fileTitle' => $tableConfig['tableTitle'],
            'insert' => [],
        ];

        if (isset($tableConfig['relations']['belongsTo'])) {

            foreach ($tableConfig['relations']['belongsTo'] as $tableAlias => $tableAliasConfig) {

                if (!array_key_exists('nullable', $tableAliasConfig)) {

                    $foreignKey = (isset($tableAliasConfig['customLinkingColumn'])) ?
                        $tableAliasConfig['customLinkingColumn'] :
                        Inflector::singularize(Inflector::underscore($tableAlias)) . "_id";

                    $propertyName = (isset($tableAliasConfig['propertyName'])) ?
                        $tableAliasConfig['propertyName'] :
                        lcfirst(Inflector::singularize(Inflector::underscore($tableAlias)));

                    $result['insert']['requiredLinkingFields'][] = [
                        $foreignKey,
                        $propertyName,
                    ];
                }
            }
        }

        foreach ($tableConfig['validations'] as $fieldTitle => $fieldConfig) {

            if (array_key_exists('requirePresence', $fieldConfig)) {
                $type = 'array';

                if (array_key_exists('scalar', $fieldConfig)) {
                    $type = 'string';
                }
                if (array_key_exists('boolean', $fieldConfig)) {
                    $type = 'bool';
                }
                if (array_key_exists('integer', $fieldConfig)) {
                    $type = 'int';
                }
                if (array_key_exists('numeric', $fieldConfig)) {
                    $type = 'float';
                }
                if (array_key_exists('dateTime', $fieldConfig)) {
                    $type = '\\DateTime';
                }

                $result['insert']['requiredFields'][] = [
                    $type,
                    $fieldTitle,
                ];
            }
        }

        return $result;
    }

}