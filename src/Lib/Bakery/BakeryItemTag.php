<?php declare(strict_types = 1);

namespace Infotechnohelp\Bakery\Lib\Bakery;

class BakeryItemTag
{
    private $tag;

    public function __construct(array $tags = [])
    {
        $this->tag = $tags;
    }

    public function getTagArray(): array
    {
        return $this->tag;
    }

    public function setTag(array $tag)
    {
        $this->tag = $tag;
    }

    public function mergeTagsBefore(BakeryItemTags $tags): self
    {
        $this->setTag(array_merge($tags->getTags(), $this->getTags()));

        return $this;
    }

    public function mergeTagsAfter(BakeryItemTags $tags): self
    {
        $this->setTag(array_merge($this->getTags(), $tags->getTags()));

        return $this;
    }
}