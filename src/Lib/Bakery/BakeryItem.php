<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Lib\Bakery;

/**
 * Class BakeryItem
 * @package Infotechnohelp\Bakery\Lib\Bakery
 */
class BakeryItem
{

    const PHP_MARKER = 1;
    const HTML_MARKER = 2;

    /**
     * @var string|null
     */
    public $title = null;

    /**
     * @var TemplateReservedNeedles
     */
    public $reservedNeedles;

    /**
     * @var BakeryItemTags
     */
    public $bakeryItemTags;

    // @todo Implement later

    /**
     * @var array
     */
    protected $bakeryItemClasses = [];

    /**
     * @var string
     */
    protected $result = '';

    /**
     * @var string
     */
    protected $needle = '#';

    /**
     * @var string
     */
    protected $configClass;

    /**
     * @var BakeryGlobalInput
     */
    protected $globalInput;

    /**
     * @param string|null $title
     */
    public function setTitle(string $title = null)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }


    public function setGlobalInput(array $input)
    {
        $this->globalInput = new BakeryGlobalInput($input);

        return $this;
    }

    public function getGlobalInput(): ?BakeryGlobalInput
    {
        return $this->globalInput;
    }

    /**
     * BakeryItem constructor.
     * @param string|null $title
     * @param string $configClass
     */
    public function __construct(string $title = null, string $configClass = BakeryConfig::class)
    {
        $this->setConfigClass($configClass);

        /** @var BakeryConfig $configClass */
        $configClass = $this->getConfigClass();

        $this->setNeedle($configClass::needle());

        $this->setTitle($title);

        $this->setGlobalInput([]);

        $this->setBakeryItemTags(new BakeryItemTags());

        $this->setReservedNeedles(new TemplateReservedNeedles());
    }

    public function setConfigClass(string $configClass)
    {
        $this->configClass = $configClass;
    }

    public function getConfigClass(): string
    {
        return $this->configClass;
    }

    public function setBakeryItemTags(BakeryItemTags $bakeryItemTags)
    {
        $this->bakeryItemTags = $bakeryItemTags;

        return $this;
    }

    public function setReservedNeedles(TemplateReservedNeedles $reservedNeedles)
    {
        $this->reservedNeedles = $reservedNeedles;

        return $this;
    }

    public function mergeReservedNeedles(TemplateReservedNeedles $reservedNeedles)
    {

        //$result = [];

        $currentReservedNeedles = $this->getReservedNeedles()->getAll();

        $receivedReservedNeedles = $reservedNeedles->getAll();

        foreach ($receivedReservedNeedles as $key => $value) {
            $currentReservedNeedle = $currentReservedNeedles[$key] ?? null;
            $currentReservedNeedles[$key] = "$currentReservedNeedle$value";
        }

        $this->setReservedNeedles(new TemplateReservedNeedles($currentReservedNeedles));

        return $this;
    }

    public function getReservedNeedles(): TemplateReservedNeedles
    {
        return $this->reservedNeedles;
    }

    public function mergeBakeryItemTagsBefore(BakeryItemTags $bakeryItemTags)
    {
        /** @var BakeryItemTags $thisBakeryItemTags */
        $thisBakeryItemTags = $this->bakeryItemTags;

        $thisBakeryItemTags->mergeTagsBefore($bakeryItemTags);

        return $this;
    }

    public function addBakeryItemTag(string $title, string $bakeryItemClass)
    {
        /** @var BakeryItemTags $thisBakeryItemTags */
        $thisBakeryItemTags = $this->bakeryItemTags;

        $thisBakeryItemTags->push(new BakeryItemTag([
            $title => $bakeryItemClass,
        ]));
    }

    public function getBakeryItemTags(): BakeryItemTags
    {
        return $this->bakeryItemTags;
    }

    public function resultContainsReservedNeedles(): bool
    {
        foreach ($this->getReservedNeedles()->getAll() as $reservedNeedleKey => $reservedNeedleValue) {
            if (strpos($this->getResult(), "{{$reservedNeedleKey}}") !== false) {
                return true;
            }
        }

        return false;
    }


    private function avoidLineRepetitions(string $input): string
    {
        $lines = (explode("\n", $input));

        return implode("\n", array_unique($lines));
    }

    public function replaceReservedNeedles(string $title = null, string $removeBeforeNeedle = null, string $removeAfterNeedle = null)
    {
        if ($title !== null) {

            $reservedNeedleValue = $this->getReservedNeedles()->getByKey($title);

            if ( $title === 'use') {
                $reservedNeedleValue = $this->avoidLineRepetitions($reservedNeedleValue);
            }

            $this->setResult(
                str_replace(
                    "$removeBeforeNeedle{{" . $title . "}}$removeAfterNeedle",
                    $reservedNeedleValue,
                    $this->getResult()
                )
            );

            return $this;
        }

        if ($this->resultContainsReservedNeedles()) {
            foreach ($this->getReservedNeedles()->getAll() as $reservedNeedleKey => $reservedNeedleValue) {

                if ($reservedNeedleKey === 'use') {
                    $reservedNeedleValue = $this->avoidLineRepetitions($reservedNeedleValue);
                }

                $this->setResult(str_replace("$removeBeforeNeedle{{" . $reservedNeedleKey . "}}$removeAfterNeedle", $reservedNeedleValue, $this->getResult()));
            }
        }

        return $this;
    }

    protected function getFullBakeryTagLabel()
    {
        $result = [];


        foreach ($this->getBakeryItemTags()->getTags() as $index => $tag) {
            /** @var $tag BakeryItemTag */
            $result[] = array_keys($tag->getTagArray())[0];
        }

        return implode('.', $result);
    }

    /**
     * @param int $type
     * @return string|null
     */
    protected function openMarker(int $type = self::PHP_MARKER): ?string
    {
        if (empty($this->getTitle())) {
            return null;
        }

        /** @var BakeryConfig $configClass */
        $configClass = $this->getConfigClass();

        if ($this instanceof Template && $configClass::markers()) {
            return ($type === self::PHP_MARKER) ? "\n/*bakery open {$this->getFullBakeryTagLabel()} */\n" : null;
        }

        return null;
    }

    /**
     * @param int $type
     * @return string|null
     */
    protected function closeMarker(int $type = self::PHP_MARKER): ?string
    {
        if (empty($this->getTitle())) {
            return null;
        }

        /** @var BakeryConfig $configClass */
        $configClass = $this->getConfigClass();

        if ($this instanceof Template && $configClass::markers()) {
            return ($type === self::PHP_MARKER) ? "/*bakery close {$this->getFullBakeryTagLabel()} */\n" : null;
        }

        return null;
    }

    public function getMarkedResult()
    {
        return
            $this->openMarker() .
            $this->getResult() .
            $this->closeMarker();
    }

    public function getResult(): string
    {
        return $this->result;
    }

    public function setResult(string $result)
    {
        $this->result = $result;
    }


    public function setNeedle(string $needle)
    {
        $this->needle = $needle;
    }

    public function getNeedle()
    {
        return $this->needle;
    }

    public function getClass()
    {
        return get_class($this);
    }
}

