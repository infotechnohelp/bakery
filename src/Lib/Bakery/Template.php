<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Lib\Bakery;

/**
 * Class Template
 * @package Infotechnohelp\Bakery\Lib\Bakery
 */
class Template extends BakeryItem
{
    /**
     * @var TemplateOptions
     */
    protected $options;

    /**
     * @var TemplateInput
     */
    protected $input;

    public function __construct(string $title = null, array $input = [], array $options = [], string $configClass = BakeryConfig::class)
    {
        parent::__construct($title, $configClass);

        $this->setOptions(new TemplateOptions($options));
        $this->setInput($input);
    }

    public function getOptions(): TemplateOptions
    {
        return $this->options;
    }

    protected function setOptions(TemplateOptions $TemplateOptions)
    {
        $this->options = $TemplateOptions;

        return $this;
    }

    public function mergeWithDefaultOptions(array $options)
    {
        $TemplateOptionsClass = get_class($this->options);

        $this->options = new $TemplateOptionsClass(array_merge($this->getOptions()->getAll(), $options));

        return $this;
    }

    public function setInput(array $input = null)
    {
        if($input === null){
            return $this;
        }

        $this->input = new TemplateInput($input);

        return $this;
    }

    public function getInput(): TemplateInput
    {
        return $this->input;
    }

    public function getResult(): string
    {
        $this->setResult($this->main());

        return $this->result;
    }
}
