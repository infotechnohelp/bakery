<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Lib\Bakery;

use Infotechnohelp\Bakery\Lib\FileWizard;
use Infotechnohelp\Bakery\Templates\CakePhp\ShellClassTemplate;
use Infotechnohelp\Bakery\Templates\Php\PhpFileTemplate;

class BakeryScope extends BakeryItem
{
    public function __construct(string $title = null, string $configClass = BakeryConfig::class)
    {
        parent::__construct($title, $configClass);

        ($title !== null) ?
            $this->addBakeryItemTag($title, self::class) : null;

        $this->setResult($this->getNeedle());
    }

    public function replaceNeedle(BakeryItem $BakeryItem, string $removeAfterNeedle = null, string $removeBeforeNeedle = null)
    {
        if (!empty($BakeryItem->getTitle())) {
            $this->addBakeryItemTag($BakeryItem->getTitle(), $BakeryItem->getClass());
        }

        if ($this->getGlobalInput() !== null) {
            $BakeryItem->setGlobalInput($this->getGlobalInput()->getAll());
        }

        $BakeryItem->setBakeryItemTags($this->getBakeryItemTags());

        $this->mergeReservedNeedles($BakeryItem->getReservedNeedles());

        $this->setResult((new FileWizard())->replaceNeedle($this->getResult(), $BakeryItem->getMarkedResult(),
            $removeBeforeNeedle . $this->getNeedle() . $removeAfterNeedle));

        return $this;
    }

    // @todo Implement those parameters later
    public function insertBeforeNeedle(BakeryItem $BakeryItem/*, string $removeAfterNeedle = null, string $removeBeforeNeedle = null*/)
    {
        if (!empty($BakeryItem->getTitle())) {
            $this->addBakeryItemTag($BakeryItem->getTitle(), $BakeryItem->getClass());
        }

        if ($this->getGlobalInput() !== null) {
            $BakeryItem->setGlobalInput($this->getGlobalInput()->getAll());
        }

        $BakeryItem->setBakeryItemTags($this->getBakeryItemTags());

        $this->mergeReservedNeedles($BakeryItem->getReservedNeedles());

        $this->setResult((new FileWizard())->replaceNeedle($this->getResult(), $BakeryItem->getMarkedResult() . $this->getNeedle(), $this->getNeedle()));

        return $this;
    }

    // @todo Implement those parameters later
    public function insertAfterNeedle(BakeryItem $BakeryItem/*, string $removeAfterNeedle = null, string $removeBeforeNeedle = null*/)
    {
        if (!empty($BakeryItem->getTitle())) {
            $this->addBakeryItemTag($BakeryItem->getTitle(), $BakeryItem->getClass());
        }

        if ($this->getGlobalInput() !== null) {
            $BakeryItem->setGlobalInput($this->getGlobalInput()->getAll());
        }

        $BakeryItem->setBakeryItemTags($this->getBakeryItemTags());

        $this->mergeReservedNeedles($BakeryItem->getReservedNeedles());

        $this->setResult((new FileWizard())->replaceNeedle($this->getResult(), $this->getNeedle() . $BakeryItem->getMarkedResult(), $this->getNeedle()));

        return $this;
    }


    public function removeNeedle(string $removeBeforeNeedle = null, string $removeAfterNeedle = null)
    {
        if ($this->configClass::markers()) {
            $removeBeforeNeedle = null;
        }

        $this->setResult(str_replace($removeBeforeNeedle . $this->getNeedle() . $removeAfterNeedle, '', $this->getResult()));

        return $this;
    }

    public function replaceNeedleWithCallback(
        BakeryItem $BakeryItem,
        callable $callback = null,
        string $removeAfterNeedle = null,
        string $removeBeforeNeedle = null
    )
    {
        $currentBakeryItemTitles = $this->getBakeryItemTags();

        if (!empty($BakeryItem->getTitle())) {
            $BakeryItem->setBakeryItemTags($currentBakeryItemTitles->mergeTagsAfter($BakeryItem->getBakeryItemTags()));
        }

        if ($this->getGlobalInput() !== null) {
            $BakeryItem->setGlobalInput($this->getGlobalInput()->getAll());
        }

        ($callback === null) ? null : $callback($BakeryItem);

        $this->mergeReservedNeedles($BakeryItem->getReservedNeedles());

        $this->setResult((new FileWizard())->replaceNeedle($this->getResult(), $BakeryItem->getMarkedResult(),
            $removeBeforeNeedle . $this->getNeedle() . $removeAfterNeedle));

        $this->setBakeryItemTags($currentBakeryItemTitles);


        return $this;
    }

    // @todo Implement those parameters later
    public function insertBeforeNeedleWithCallback(
        BakeryItem $BakeryItem,
        callable $callback = null
        /*, string $removeAfterNeedle = null, string $removeBeforeNeedle = null*/
    )
    {
        $currentBakeryItemTitles = $this->getBakeryItemTags();

        if (!empty($BakeryItem->getTitle())) {
            $this->addBakeryItemTag($BakeryItem->getTitle(), $BakeryItem->getClass());
        }

        if ($this->getGlobalInput() !== null) {
            $BakeryItem->setGlobalInput($this->getGlobalInput()->getAll());
        }

        ($callback === null) ? null : $callback($BakeryItem);

        $this->mergeReservedNeedles($BakeryItem->getReservedNeedles());

        $this->setResult((new FileWizard())->replaceNeedle($this->getResult(), $BakeryItem->getMarkedResult() . $this->getNeedle(), $this->getNeedle()));

        $this->setBakeryItemTags($currentBakeryItemTitles);

        return $this;
    }

}