<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Lib\Bakery;

interface BakeryContainerInterface
{
    public function render();
}