<?php declare(strict_types = 1);

namespace Infotechnohelp\Bakery\Lib\Bakery;


class BakeryContainer
{
    protected $input;

    public function setInput(array $input)
    {
        $this->input = new TemplateInput($input);

        return $this;
    }

    public function getInput(): TemplateInput
    {
        return $this->input;
    }
}