<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Lib\Bakery;

/**
 * Class TemplateReservedNeedles
 * @package Infotechnohelp\Bakery\Lib\Bakery
 */
class TemplateReservedNeedles
{
    /**
     * @var array
     */
    private $needles = [];

    /**
     * TemplateOptions constructor.
     * @param array $needles
     */
    public function __construct(array $needles = [])
    {
        $this->needles = $needles;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->needles;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getByKey(string $key)
    {
        return $this->needles[$key];
    }

    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function setByKey(string $key, string $value)
    {
        $this->needles[$key] = $value;

        return $this;
    }
}

