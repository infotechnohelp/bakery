<?php declare(strict_types = 1);

namespace Infotechnohelp\Bakery\Lib\Bakery;

/**
 * Class TemplateOptions
 * @package Infotechnohelp\Bakery\Lib\Bakery
 */
class TemplateOptions
{
    /**
     * @var array
     */
    private $options = [];

    /**
     * TemplateOptions constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->options;
    }

    /**
     * @param string $key
     * @return mixed
     */
    protected function getByKey(string $key)
    {
        return $this->options[$key];
    }
}

