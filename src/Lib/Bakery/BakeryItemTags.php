<?php declare(strict_types = 1);

namespace Infotechnohelp\Bakery\Lib\Bakery;

class BakeryItemTags
{
    private $tags;

    public function __construct(array $tags = [])
    {
        $this->tags = $tags;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function getFirstTag(): BakeryItemTag
    {
        return $this->tags[0];
    }

    public function setTags(array $tags)
    {
        $this->tags = $tags;
    }

    public function mergeTagsBefore(BakeryItemTags $tags): self
    {
        $this->setTags(array_merge($tags->getTags(), $this->getTags()));

        return $this;
    }

    public function mergeTagsAfter(BakeryItemTags $tags): self
    {
        $this->setTags(array_merge($this->getTags(), $tags->getTags()));

        return $this;
    }

    public function push(BakeryItemTag $tag): self
    {
        $result = $this->getTags();
        $result[] = $tag;
        $this->setTags($result);

        return $this;
    }
}