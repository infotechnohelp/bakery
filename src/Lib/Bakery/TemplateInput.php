<?php

namespace Infotechnohelp\Bakery\Lib\Bakery;

class TemplateInput
{
    /**
     * @var array
     */
    private $input = [];

    /**
     * TemplateInput constructor.
     * @param array $input
     */
    public function __construct(array $input = [])
    {
        $this->input = $input;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->input;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getByKey(string $key)
    {
        return $this->input[$key] ?? null;
    }
}