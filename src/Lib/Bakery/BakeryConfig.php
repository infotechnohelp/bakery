<?php declare(strict_types = 1);

namespace Infotechnohelp\Bakery\Lib\Bakery;

use Symfony\Component\Yaml\Yaml;

class BakeryConfig
{
    public static function needle()
    {
        $mainYamlPath =
            dirname(dirname(dirname(dirname(dirname(dirname(__DIR__)))))) . DIRECTORY_SEPARATOR .
            "config" . DIRECTORY_SEPARATOR .
            "Bakery" . DIRECTORY_SEPARATOR .
            "settings" . DIRECTORY_SEPARATOR .
            "main.yml";

        if(file_exists($mainYamlPath) && array_key_exists('needle', Yaml::parse(file_get_contents($mainYamlPath)))){
            return Yaml::parse(file_get_contents($mainYamlPath))['needle'];
        }
        
        $defaultMainYamlPath =
            dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR .
            "config" . DIRECTORY_SEPARATOR .
            "settings" . DIRECTORY_SEPARATOR .
            "main.yml";


        return Yaml::parse(file_get_contents($defaultMainYamlPath))['needle'];
    }

    public static function markers()
    {
        $mainYamlPath =
            dirname(dirname(dirname(dirname(dirname(dirname(__DIR__)))))) . DIRECTORY_SEPARATOR .
            "config" . DIRECTORY_SEPARATOR .
            "Bakery" . DIRECTORY_SEPARATOR .
            "settings" . DIRECTORY_SEPARATOR .
            "main.yml";
        
        if(file_exists($mainYamlPath) && array_key_exists('markers', Yaml::parse(file_get_contents($mainYamlPath)))){
            return Yaml::parse(file_get_contents($mainYamlPath))['markers'];
        }
        
        $defaultMainYamlPath =
            dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR .
            "config" . DIRECTORY_SEPARATOR .
            "settings" . DIRECTORY_SEPARATOR .
            "main.yml";
        
        return Yaml::parse(file_get_contents($defaultMainYamlPath))['markers'];
    }
}