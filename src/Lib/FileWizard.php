<?php declare(strict_types = 1);

namespace Infotechnohelp\Bakery\Lib;

/**
 * Class FileWizard
 * @package Infotechnohelp\Bakery\Lib
 */
class FileWizard
{
    public function replaceNeedle(string $stack, string $patch, string $needle = '#'): string
    {
        if (substr_count($stack, $needle) === 0) {
            throw new \Exception("No needle '$needle' found in:\n$stack");
        }

        if (substr_count($stack, $needle) > 1) {
            throw new \Exception("Too many needles '$needle' found in:\n$stack");
        }

        return str_replace($needle, $patch, $stack);

    }
}


