<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Containers\CakePhp;

use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainer;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainerInterface;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryScope;
use Infotechnohelp\Bakery\Templates\CakePhp\ShellClassTemplate;
use Infotechnohelp\Bakery\Templates\Php\MethodTemplate;
use Infotechnohelp\Bakery\Templates\Php\PhpFileTemplate;
use Infotechnohelp\Bakery\Templates\StaticTemplate;

/**
 * Class BasicShellFileContainer
 * @package Infotechnohelp\Bakery\Containers\CakePhp
 */
class BasicShellFileContainer extends BakeryContainer implements BakeryContainerInterface
{
    /**
     * @return string
     */
    public function render()
    {
        $BasicShellScope = new BakeryScope($this->getInput()->getByKey('fileTitle'));

        $BasicShellScope
            ->setGlobalInput(['namespace' => 'App\\Shell'])
            ->replaceNeedle(
                (new PhpFileTemplate())
                    ->mergeWithDefaultOptions([/*'strictType' => false,*/ 'namespace' => true, /*, 'use' => false*/])
            )
            ->replaceNeedle((new ShellClassTemplate('ShellClass')))
            ->replaceNeedleWithCallback(new BakeryScope(),
                function (BakeryScope $Scope) {
                    $Scope
                        ->insertBeforeNeedle(
                            (new BakeryScope())
                                ->replaceNeedle(new MethodTemplate('main'))
                                ->replaceNeedle(
                                    (new StaticTemplate(
                                        "\$this->output('Hello world');",
                                        'main method body'
                                    ))
                                )
                        );
                }
            )
            ->removeNeedle("\n\n")
            ->replaceReservedNeedles('use', null, "\n");


        return $BasicShellScope->getMarkedResult();
    }
}