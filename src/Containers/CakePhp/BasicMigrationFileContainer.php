<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Containers\CakePhp;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainer;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainerInterface;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryItemTags;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryScope;
use Infotechnohelp\Bakery\Lib\Parser\CakePhpModelSchema;
use Infotechnohelp\Bakery\Templates\CakePhp\Migration\ClassTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Migration\ColumnTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Migration\DownMethodTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Migration\LinkingColumnTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Migration\LinkingTableTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Migration\TableMethodTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Migration\TimestampColumnsTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Migration\UpMethodTemplate;
use Infotechnohelp\Bakery\Templates\Php\MethodTemplate;
use Infotechnohelp\Bakery\Templates\Php\PhpFileTemplate;

/**
 * Class BasicMigrationFileContainer
 * @package Infotechnohelp\Bakery\Containers\CakePhp
 */
class BasicMigrationFileContainer extends BakeryContainer implements BakeryContainerInterface
{
    /**
     * @var string
     */
    private $yamlConfigPath;

    /**
     * @param string $yamlConfigPath
     * @return $this
     */
    public function setYamlConfigPath(string $yamlConfigPath)
    {
        $this->yamlConfigPath = $yamlConfigPath;

        return $this;
    }

    /**
     * @return string
     */
    public function getYamlConfigPath(): string
    {
        return $this->yamlConfigPath;
    }




    /**
     * @return string
     */
    public function render()
    {
        $linkingTables = [];

        $createdTables = [];

        $migrationConfig = CakePhpModelSchema::getMigrationConfig($this->getYamlConfigPath());

        // CALLBACK
        $tableMethodBodyCallback = function (BakeryScope $Scope, BakeryItemTags $currentBakeryItemTags, array $tableConfig)
        use ($migrationConfig, &$linkingTables) {

            if (!empty($tableConfig['table']['linking']['columns'])) {
                foreach ($tableConfig['table']['linking']['columns'] as $linkedTable => $linkedTableConfig) {
                    
                    $columnTitle = Inflector::underscore(Inflector::singularize($linkedTable)) . '_id';
                    
                    if(array_key_exists('customLinkingColumn', $linkedTableConfig)){
                        $columnTitle = $linkedTableConfig['customLinkingColumn'];
                    }
                    
                    $Scope
                        ->insertBeforeNeedle(
                            (new BakeryScope())
                                ->mergeBakeryItemTagsBefore($currentBakeryItemTags)
                                ->replaceNeedle(
                                    (new LinkingColumnTemplate($columnTitle))
                                        ->setInput([
                                            'columnTitle' => $columnTitle,
                                            'options' => $linkedTableConfig,
                                        ])
                                )
                        );
                }
            }


            $columns = $tableConfig['columns'];

            $timestampBehaviour = true;

            foreach ($columns as $columnConfig) {

                if ($columnConfig['title'] === 'timestampBehaviour') {
                    $timestampBehaviour = $columnConfig['options'];
                    continue;
                }
                
                $columnTitle = $columnConfig['title'];
                
                $Scope
                    ->insertBeforeNeedle(
                        (new BakeryScope())
                            ->mergeBakeryItemTagsBefore($currentBakeryItemTags)
                            ->replaceNeedle(
                                (new ColumnTemplate($columnTitle))
                                    ->setInput([
                                        'columnTitle' => $columnTitle,
                                        'type' => CakePhpModelSchema::prepareType($columnConfig['type']),
                                        'options' => $columnConfig['options'],
                                    ])
                            )
                    );
            }

            if ($timestampBehaviour) {
                $Scope
                    ->insertBeforeNeedle(
                        (new BakeryScope())
                            ->mergeBakeryItemTagsBefore($currentBakeryItemTags)
                            ->replaceNeedle(new TimestampColumnsTemplate('timestamp'))
                    );
            }


            $Scope->removeNeedle();


            $title = $tableConfig['table']['tableTitle'];

            foreach ($tableConfig['table']['linking']['tables'] as $linkedTable => $linkedTableConfig) {

                $tableTitles = [$title, $linkedTable];
                sort($tableTitles);

                $tableTitle = implode('', $tableTitles);

                $linkingTables[$tableTitle]['tables'] = $tableTitles;

                $linkingTables[$tableTitle]['uniqueFieldGroups'] = [];

                if(array_key_exists('uniqueFieldGroups', $linkedTableConfig)){
                    $linkingTables[$tableTitle]['uniqueFieldGroups'] = $linkedTableConfig['uniqueFieldGroups'];
                }

            }
        };


        // CALLBACK
        $classBodyCallback = function (BakeryScope $Scope, BakeryItemTags $currentBakeryItemTags)

        use ($migrationConfig, $tableMethodBodyCallback, &$linkingTables, &$createdTables) {


            foreach ($migrationConfig as $tableConfig) {

                $tableTitle = $tableConfig['table']['tableTitle'];

                $createdTables[] = $tableTitle;

                $internalScope = (new BakeryScope());

                $uniqueFieldGroups = $tableConfig['uniqueFieldGroups'] ?? [];

                $Scope
                    ->insertBeforeNeedle(

                        $internalScope
                            ->mergeBakeryItemTagsBefore($currentBakeryItemTags)
                            ->replaceNeedle(
                                (new MethodTemplate($tableTitle))
                                    ->mergeWithDefaultOptions(['access' => 'private'])
                            )
                            ->replaceNeedle(
                                (new TableMethodTemplate(Inflector::underscore($tableTitle)))
                                    ->setInput([
                                        'underscored_table_title' => Inflector::underscore($tableTitle),
                                        'uniqueFieldGroups' => $uniqueFieldGroups,
                                    ])
                            )
                            ->replaceNeedleWithCallback(new BakeryScope(),
                                function (BakeryScope $NewScope) use ($internalScope, $tableMethodBodyCallback, $tableConfig) {
                                    $tableMethodBodyCallback($NewScope, clone $internalScope->getBakeryItemTags(), $tableConfig);
                                }
                            )
                    );
            }


            foreach ($linkingTables as $linkingTable => $tableTitles) {

                $Scope
                    ->insertBeforeNeedle(
                        (new BakeryScope())
                            ->mergeBakeryItemTagsBefore($currentBakeryItemTags)
                            ->replaceNeedle(new MethodTemplate($linkingTable))
                            ->replaceNeedle(
                                (new TableMethodTemplate(Inflector::underscore($linkingTable)))
                                    ->setInput(['underscored_table_title' => Inflector::underscore($linkingTable)])
                            )
                            ->replaceNeedle(
                                (new LinkingTableTemplate())
                                    ->setInput([
                                        'linkedTables' => $tableTitles['tables'],
                                        'uniqueFieldGroups' => $tableTitles['uniqueFieldGroups'],
                                    ])
                            )
                    );

            }


            $allTables = $createdTables;

            foreach ($linkingTables as $title => $tables) {
                $allTables[] = $title;
            }


            $Scope
                ->insertBeforeNeedle(
                    (new BakeryScope())
                        ->mergeBakeryItemTagsBefore($currentBakeryItemTags)
                        ->replaceNeedle(new MethodTemplate('up'))
                        ->replaceNeedle(
                            (new UpMethodTemplate())
                                ->setInput(['tables' => $allTables]),
                            "\n"
                        )
                )
                ->insertBeforeNeedle(
                    (new BakeryScope())
                        ->mergeBakeryItemTagsBefore($currentBakeryItemTags)
                        ->replaceNeedle(new MethodTemplate('down'))
                        ->replaceNeedle(
                            (new DownMethodTemplate())
                                ->setInput(['tables' => $allTables]),
                            "\n"

                        )
                )
                ->removeNeedle("\n\n");
        };


        // MAIN

        $BasicShellScope = new BakeryScope($this->getInput()->getByKey('classTitle'));

        $BasicShellScope
            ->replaceNeedle(new PhpFileTemplate())
            ->replaceNeedle((new ClassTemplate('MigrationClass')))
            ->replaceReservedNeedles(null, null, "\n")
            ->replaceNeedleWithCallback(new BakeryScope('class body'),
                function (BakeryScope $Scope) use ($BasicShellScope, $classBodyCallback) {
                    $classBodyCallback($Scope, clone $BasicShellScope->getBakeryItemTags());
                }
            );

        return $BasicShellScope->getMarkedResult();
    }
}