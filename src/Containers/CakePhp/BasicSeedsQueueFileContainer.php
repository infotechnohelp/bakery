<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Containers\CakePhp;

use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainer;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainerInterface;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryScope;
use Infotechnohelp\Bakery\Templates\CakePhp\Seeds\BasicSeedsQueueTemplate;
use Infotechnohelp\Bakery\Templates\Php\PhpFileTemplate;

/**
 * Class BasicSeedsQueueFileContainer
 * @package Infotechnohelp\Bakery\Containers\CakePhp
 */
class BasicSeedsQueueFileContainer extends BakeryContainer implements BakeryContainerInterface
{

    public function render()
    {
        $input = $this->getInput()->getAll();


        $ContainerScope = new BakeryScope();


        $ContainerScope
            ->replaceNeedle(
                (new PhpFileTemplate())
                    ->mergeWithDefaultOptions(['use' => false])
            )
            ->replaceNeedle(
                (new BasicSeedsQueueTemplate('_Queue'))
                    ->setInput($input)
            );

        return $ContainerScope->getMarkedResult();
    }
}