<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Containers\CakePhp;

use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainer;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainerInterface;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryScope;
use Infotechnohelp\Bakery\Templates\CakePhp\Entity\EntityClassBodyTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Entity\EntityClassTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Entity\EntityConstantsTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Entity\EntityVirtualFieldsTemplate;
use Infotechnohelp\Bakery\Templates\FileSpecificTemplate;
use Infotechnohelp\Bakery\Templates\Php\PhpFileTemplate;
use Infotechnohelp\Bakery\Templates\StaticTemplate;

/**
 * Class BasicEntityFileContainer
 * @package Infotechnohelp\Bakery\Containers\CakePhp
 */
class BasicEntityFileContainer extends BakeryContainer implements BakeryContainerInterface
{

    public function render()
    {
        $globalInput = ['namespace' => 'App\\Model\\Entity'];

        $timestampBehaviour = $this->getInput()->getByKey('timestampBehaviour');

        $fileTitle = $this->getInput()->getByKey('fileTitle');

        $ContainerScope = new BakeryScope($fileTitle);

        $ContainerScope
            ->setGlobalInput(
                $globalInput
            )
            ->replaceNeedle(
                (new PhpFileTemplate())
                    ->mergeWithDefaultOptions(['namespace' => true])
            )
            ->insertBeforeNeedle(new FileSpecificTemplate(null, ['src/Model/Entity', 'use']))
            ->insertAfterNeedle(new FileSpecificTemplate(null, ['src/Model/Entity', 'afterClass']))
            ->replaceNeedle((new EntityClassTemplate()))
            ->insertBeforeNeedle(
                (new EntityConstantsTemplate())
                    ->setInput($this->getInput()->getByKey('data'))
            )
            ->insertBeforeNeedle(
                (new EntityClassBodyTemplate())
                    ->setInput([
                        'timestampBehaviour' => $timestampBehaviour,
                        'columns' => $this->getInput()->getByKey('columns'),
                    ])
            )
            ->insertBeforeNeedle(new StaticTemplate("\n"))
            ->insertBeforeNeedle(
                (new EntityVirtualFieldsTemplate())
                    ->setInput($this->getInput()->getByKey('virtual'))
            )
            ->replaceNeedle(new FileSpecificTemplate(null, ['src/Model/Entity', 'classBody']))
            ->replaceReservedNeedles('use', null, "\n");

        return $ContainerScope->getMarkedResult();
    }
}