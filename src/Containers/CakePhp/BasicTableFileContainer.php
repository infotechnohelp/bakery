<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Containers\CakePhp;

use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainer;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainerInterface;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryItemTags;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryScope;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateReservedNeedles;
use Infotechnohelp\Bakery\Templates\CakePhp\Table\BuildRulesMethodBodyTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Table\InitializeMethodAddRelationsTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Table\InitializeMethodBodyTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Table\InitializeMethodRelationsTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Table\TableClassTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Table\ValidationDefaultMethodBodyTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Table\ValidationDefaultMethodValidationsTemplate;
use Infotechnohelp\Bakery\Templates\FileSpecificTemplate;
use Infotechnohelp\Bakery\Templates\Php\MethodTemplate;
use Infotechnohelp\Bakery\Templates\Php\PhpFileTemplate;
use Infotechnohelp\Bakery\Templates\StaticTemplate;

/**
 * Class BasicTableFileContainer
 * @package Infotechnohelp\Bakery\Containers\CakePhp
 */
class BasicTableFileContainer extends BakeryContainer implements BakeryContainerInterface
{

    public function render()
    {

        $relations = $this->getInput()->getByKey('relations');
        $addRelations = $this->getInput()->getByKey('add-relations');
        $validations = $this->getInput()->getByKey('validations');
        $buildRules = $this->getInput()->getByKey('buildRules');
        $additional = $this->getInput()->getByKey('additional') ?? [];

        // CALLBACK
        $classBodyCallback = function (BakeryScope $Scope, BakeryItemTags $currentBakeryItemTags)
        use ($relations, $addRelations, $validations, $buildRules, $additional) {

            $Scope
                ->insertBeforeNeedle(
                    (new BakeryScope())
                        ->setGlobalInput($Scope->getGlobalInput()->getAll())
                        ->mergeBakeryItemTagsBefore($currentBakeryItemTags)
                        ->replaceNeedle(
                            (new MethodTemplate('initialize'))
                                ->mergeWithDefaultOptions(['docs' => true])
                                ->setInput([
                                    'parameters' => [
                                        ['array', 'config', null],
                                    ],
                                ])
                        )
                        ->replaceNeedle(
                            (new InitializeMethodBodyTemplate())
                        )
                        ->insertBeforeNeedle(
                            (new InitializeMethodRelationsTemplate())
                                ->setInput($relations)
                        )
                        ->insertBeforeNeedle(
                            (new InitializeMethodAddRelationsTemplate())
                                ->setInput($addRelations)
                        )
                        ->replaceNeedle(new FileSpecificTemplate(null, ['src/Model/Table', 'initialize']))

                )
                ->insertBeforeNeedle(
                    (new BakeryScope())
                        ->mergeBakeryItemTagsBefore($currentBakeryItemTags)
                        ->replaceNeedle(
                            (new MethodTemplate('validationDefault'))
                                // @todo Move to template
                                ->mergeReservedNeedles(
                                    (new TemplateReservedNeedles())->setByKey(
                                        'use',
                                        "use Cake\\Validation\\Validator;\n"
                                    )
                                )
                                ->mergeWithDefaultOptions(['docs' => true])
                                ->setInput([
                                    'parameters' => [
                                        ['Validator', 'validator', null],
                                    ],
                                    'return' => 'Validator',
                                ])
                        )
                        ->replaceNeedle(
                            (new ValidationDefaultMethodBodyTemplate())
                        )
                        ->replaceNeedle(
                            (new ValidationDefaultMethodValidationsTemplate(null,$validations ?? []))
                        )
                );

            if (!empty($buildRules) || !empty($additional)) {

                $Scope->replaceNeedleWithCallback(new BakeryScope(),
                    function (BakeryScope $NewScope) use ($Scope, $buildRules, $additional) {

                        $NewScope
                            ->insertBeforeNeedle(
                                (new BakeryScope())
                                    ->mergeBakeryItemTagsBefore($Scope->getBakeryItemTags())
                                    ->replaceNeedle(
                                        (new MethodTemplate('buildRules'))
                                            ->mergeWithDefaultOptions(['docs' => true])
                                            ->setInput([
                                                'parameters' => [
                                                    ['RulesChecker', 'rules', null],
                                                ],
                                                'return' => 'RulesChecker',
                                            ])
                                    )
                                    ->replaceNeedle(new BuildRulesMethodBodyTemplate(null, [$buildRules, $additional]))
                            );
                    }
                );

            }

            $Scope->insertBeforeNeedle(new FileSpecificTemplate(null, ['src/Model/Table', 'classBody']));

            $Scope->removeNeedle();

        };


        // MAIN
        $ContainerScope = new BakeryScope($this->getInput()->getByKey('tableTitle'));

        $timestampBehaviour = $this->getInput()->getByKey('timestampBehaviour');

        $globalInput = ['namespace' => 'App\\Model\\Table'];

        if ($timestampBehaviour) {
            $globalInput['timestampBehaviour'] = true;
        }

        $ContainerScope
            ->setGlobalInput(
                $globalInput
            )
            ->replaceNeedle(
                (new PhpFileTemplate())
                    ->mergeWithDefaultOptions(['namespace' => true])
            )
            ->insertBeforeNeedle(new FileSpecificTemplate(null, ['src/Model/Table', 'use']))
            ->replaceNeedle((new TableClassTemplate()))
            ->replaceNeedleWithCallback(new BakeryScope('class body'),
                function (BakeryScope $Scope) use ($ContainerScope, $classBodyCallback) {
                    $classBodyCallback($Scope, clone $ContainerScope->getBakeryItemTags());
                },
                "\n"
            )
            ->replaceReservedNeedles('use', null, "\n");


        return $ContainerScope->getMarkedResult();
    }

}