<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Containers\CakePhp;


use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainer;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainerInterface;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryScope;
use Infotechnohelp\Bakery\Templates\CakePhp\ModelManager\ModelManagerClassContentsTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\ModelManager\ModelManagerClassTemplate;
use Infotechnohelp\Bakery\Templates\CakePhp\Table\TableClassTemplate;
use Infotechnohelp\Bakery\Templates\FileSpecificTemplate;
use Infotechnohelp\Bakery\Templates\Php\PhpFileTemplate;
use Infotechnohelp\ModelManager\ModelManager;

/**
 * Class BasicModelManagerContainer
 * @package Infotechnohelp\Bakery\Containers\CakePhp
 */
class BasicModelManagerContainer extends BakeryContainer implements BakeryContainerInterface
{

    public function render()
    {
        $globalInput = ['namespace' => 'App\\ModelManager'];

        $fileTitle = $this->getInput()->getByKey('fileTitle');
        


        $ContainerScope = new BakeryScope($fileTitle);

        $ContainerScope
            ->setGlobalInput(
                $globalInput
            )
            ->replaceNeedle(
                (new PhpFileTemplate())
                    ->mergeWithDefaultOptions(['namespace' => true])
            )
            ->insertBeforeNeedle(new FileSpecificTemplate(null, ['src/ModelManager', 'use']))
            ->replaceNeedle((new ModelManagerClassTemplate()))
            ->insertBeforeNeedle(
                (new ModelManagerClassContentsTemplate())
                ->setInput($this->getInput()->getAll())
            )
            ->replaceNeedle(new FileSpecificTemplate(null, ['src/ModelManager', 'classContents']))
            ->replaceReservedNeedles('use', null, "\n");

        return $ContainerScope->getMarkedResult();
    }
}