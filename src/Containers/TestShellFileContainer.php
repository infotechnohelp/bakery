<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Containers;

use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainer;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainerInterface;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryItemTags;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryScope;
use Infotechnohelp\Bakery\Templates\CakePhp\ShellClassTemplate;
use Infotechnohelp\Bakery\Templates\Php\MethodTemplate;
use Infotechnohelp\Bakery\Templates\Php\PhpFileTemplate;
use Infotechnohelp\Bakery\Templates\StaticTemplate;
use Infotechnohelp\Bakery\Templates\Test\TestTemplate;

/**
 * Class TestShellFileContainer
 * @package Infotechnohelp\Bakery\Containers
 */
class TestShellFileContainer extends BakeryContainer implements BakeryContainerInterface
{
    /**
     * @return string
     */
    public function render()
    {
        $classBodyCallback = function (BakeryScope $Scope, BakeryItemTags $currentBakeryItemTags) {

            $Scope
                ->insertBeforeNeedle(
                    (new BakeryScope())
                        ->mergeBakeryItemTagsBefore($currentBakeryItemTags)
                        ->replaceNeedle(new MethodTemplate('main method'))
                        ->replaceNeedle(new TestTemplate('main method body'))
                )
                ->insertBeforeNeedle(
                    (new BakeryScope())
                        ->mergeBakeryItemTagsBefore($currentBakeryItemTags)
                        ->replaceNeedle(
                            (new MethodTemplate('test method'))
                                ->setInput([
                                    'parameters' => [
                                        ['string', 'input', 'null'],
                                        ['array', 'options', '[]'],
                                    ],
                                    'return' => 'string',
                                ])
                        )
                        ->replaceNeedle(
                            (new StaticTemplate(
                                "/** Test output\n" .
                                "* Body here\n" .
                                "*/",
                                'test method body'
                            ))
                        )
                )
                ->removeNeedle("\n\n");
        };


        $BakeMigrationShell = new BakeryScope($this->getInput()->getByKey('fileTitle'));

        $BakeMigrationShell
            ->replaceNeedle(
                (new PhpFileTemplate())
                    ->mergeWithDefaultOptions([/*'strictType' => false,*/ 'namespace' => true, /*, 'use' => false*/])
                    ->setInput(['namespace' => 'App\\Shell'])
            )
            ->replaceNeedle((new ShellClassTemplate('ShellClass')))
            ->replaceNeedleWithCallback(new BakeryScope('class body'),
                function (BakeryScope $Scope) use ($BakeMigrationShell, $classBodyCallback) {
                    $classBodyCallback($Scope, clone $BakeMigrationShell->getBakeryItemTags());
                }
            )
            ->replaceReservedNeedles('use', null, "\n");


        return $BakeMigrationShell->getMarkedResult();
    }
}