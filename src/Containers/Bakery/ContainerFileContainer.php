<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Containers\Bakery;

use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainer;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryContainerInterface;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryScope;
use Infotechnohelp\Bakery\Templates\Bakery\ContainerClassTemplate;
use Infotechnohelp\Bakery\Templates\Php\MethodTemplate;
use Infotechnohelp\Bakery\Templates\Php\PhpFileTemplate;

/**
 * Class ContainerFileContainer
 * @package Infotechnohelp\Bakery\Containers\Bakery
 */
class ContainerFileContainer extends BakeryContainer implements BakeryContainerInterface
{
    /**
     * @return string
     */
    public function render()
    {
        $ContainerScope = new BakeryScope($this->getInput()->getByKey('fileTitle'));

        $ContainerScope
            ->setGlobalInput(['namespace' => 'Infotechnohelp\\Bakery\\Containers'])
            ->replaceNeedle(
                (new PhpFileTemplate())
                    ->mergeWithDefaultOptions(['namespace' => true])
            )
            ->replaceNeedle((new ContainerClassTemplate()))
            ->replaceNeedle(new MethodTemplate('render'), "\n\n")
            ->removeNeedle("\n\n")
            ->replaceReservedNeedles('use', null, "\n");

        return $ContainerScope->getMarkedResult();
    }
}