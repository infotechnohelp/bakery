<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\Php;

use Cake\Validation\Validator;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateReservedNeedles;

/**
 * Class MethodTemplate
 * @package Infotechnohelp\Bakery\Templates\Php
 */
class MethodTemplate extends Template implements TemplateInterface
{
    public function __construct(string $title = null)
    {
        parent::__construct($title);

        $this->setOptions(new MethodTemplateOptions([
            'access' => 'public',
            'static' => false,
            // @todo Change later to be true
            'docs' => false,
        ]));

        $this->getReservedNeedles()->setByKey("use", "use Cake\\ORM\\RulesChecker;\n");
    }

    /**
     * @param array|null $parameters
     * @return string
     */
    private function renderParameters(array $parameters = null): string
    {
        $result = '';

        if (!empty($parameters)) {
            $i = 0;

            foreach ($parameters as $parameter) {
                $i++;

                list($type, $title, $default) = $parameter;

                $result .= "$type $$title";

                if (!empty($default)) {
                    $result .= " = $default";
                }

                if ($i !== count($parameters)) {
                    $result .= ", ";
                }
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function main()
    {
        /** @var MethodTemplateOptions $options */
        $options = $this->getOptions();

        $parameters = $this->getInput()->getByKey('parameters');

        $return = $this->getInput()->getByKey('return');

        $result = '';

        if ($options->getDocs()) {
            $result .= "/**\n";

            foreach ($parameters as $parameter) {

                list($type, $title) = $parameter;

                $result .= " * @param $type $$title\n";
            }

            if (!empty($return)) {
                $result .= " * @return $return\n";
            }

            $result .= " */\n";

        }

        $result .=
            "{$options->getAccess()} function {$this->getTitle()}(" .
            $this->renderParameters($parameters) .
            ")";

        if (!empty($return)) {
            $result .= ": $return ";
        }

        $result .=
            "{\n" .
            "\n" .
            $this->needle . "\n" .
            "\n" .
            "}\n" .
            "\n";

        return $result;
    }

    /**
     * @return array
     */
    public function inputExample()
    {
        return [
            'parameters' => [
                ['string', 'input', 'null'],
                ['array', 'options', '[]'],
            ],
            'return' => 'string',
        ];
    }
}