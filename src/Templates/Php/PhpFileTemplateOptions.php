<?php declare(strict_types = 1);

namespace Infotechnohelp\Bakery\Templates\Php;

use Infotechnohelp\Bakery\Lib\Bakery\TemplateOptions;

class PhpFileTemplateOptions extends TemplateOptions
{
    public function getStrictType()
    {
        return $this->getByKey('strictType');
    }

    public function getNamespace()
    {
        return $this->getByKey('namespace');
    }

    public function getUse()
    {
        return $this->getByKey('use');
    }
}