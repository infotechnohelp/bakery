<?php declare(strict_types = 1);

namespace Infotechnohelp\Bakery\Templates\Php;

use Infotechnohelp\Bakery\Lib\Bakery\TemplateOptions;

class MethodTemplateOptions extends TemplateOptions
{
    public function getAccess()
    {
        return $this->getByKey('access');
    }

    public function getStatic()
    {
        return $this->getByKey('static');
    }

    public function getDocs()
    {
        return $this->getByKey('docs');
    }
}