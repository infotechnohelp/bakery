<?php declare(strict_types = 1);

namespace Infotechnohelp\Bakery\Templates\Php;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class PhpFileTemplate extends Template implements TemplateInterface
{
    public function __construct(string $title = null)
    {
        parent::__construct($title);

        $this->setOptions(new PhpFileTemplateOptions([
            'strictType' => true,
            'namespace' => false,
            'use' => true,
        ]));
    }

    public function mergeWithDefaultOptions(array $options)
    {
        /** @var PhpFileTemplateOptions $thisOptions */
        $thisOptions = $this->options;

        $this->options = new PhpFileTemplateOptions(array_merge($thisOptions->getAll(), $options));

        return $this;
    }

    public function main()
    {
        /** @var PhpFileTemplateOptions $options */
        $options = $this->getOptions();

        $strictType = ($options->getStrictType()) ? " declare(strict_types = 1);" : null;

        $result =
            "<?php$strictType\n" .
            "\n";

        if ($options->getNamespace()) {
            $result .=
                "namespace {$this->getGlobalInput()->getByKey('namespace')};\n" .
                "\n";
        }

        if ($options->getUse()) {
            $result .=
                "{{use}}\n" .
                "\n";
        }

        $result .=
            $this->needle;

        return $result;
    }

    public function inputExample()
    {
        return [
            'namespace' => 'App\\Shell'
        ];
    }
}
