<?php declare(strict_types = 1);

namespace Infotechnohelp\Bakery\Templates;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

/**
 * Class StaticTemplate
 * @package Infotechnohelp\Bakery\Templates
 */
class StaticTemplate extends Template implements TemplateInterface
{
    private $output = '';

    public function __construct(string $output, string $title = null)
    {
        parent::__construct($title);

        $this->setOutput($output);
    }


    public function setOutput(string $output)
    {
        $this->output = $output;

        return $this;
    }

    public function main()
    {
        $result = $this->output;

        return $result;
    }
}