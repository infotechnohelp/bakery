<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates;

use Infotechnohelp\Bakery\Lib\Bakery\BakeryConfig;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

/**
 * Class FileSpecificTemplate
 * @package Infotechnohelp\Bakery\Templates
 */
class FileSpecificTemplate extends Template implements TemplateInterface
{
    const DS = DIRECTORY_SEPARATOR;

    private $fileSpecificTemplatePath = null;

    public function __construct(string $title = null, array $input = [], array $options = [], string $configClass = BakeryConfig::class)
    {
        parent::__construct($title, $input, $options, $configClass);

        $this->fileSpecificTemplatePath =
            dirname(dirname(dirname(dirname(dirname(__DIR__))))) . self::DS . 
            'config' . self::DS . 'Bakery' . self::DS ."FileSpecificTemplates";
        
    }

    public function main()
    {
        // @todo Create a separate method for that
        $fileTitle = array_keys($this->getBakeryItemTags()->getFirstTag()->getTagArray())[0];

        list($fileType, $section) = $this->getInput()->getAll();

        $filePath =
            $this->fileSpecificTemplatePath . self::DS . $fileType . self::DS . $fileTitle . self::DS . $section . ".txt";
        
        if(file_exists($filePath)){
            return file_get_contents($filePath);
        }

        return "";
    }
}