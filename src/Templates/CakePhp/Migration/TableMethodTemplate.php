<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Migration;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class TableMethodTemplate extends Template implements TemplateInterface
{
    public function main()
    {
        $result =
            "\$table = \$this->table('{$this->getInput()->getByKey('underscored_table_title')}', ['collation' => 'utf8mb4_unicode_ci']);\n" .
            "\$table\n" .
            "{$this->needle}\n";


        $uniqueFieldGroups = $this->getInput()->getByKey('uniqueFieldGroups') ?? [];

        foreach ($uniqueFieldGroups as $group) {
            $result .=
                "->addIndex([";

            foreach ($group as $fieldTitle) {
                $result .= "'$fieldTitle', ";
            }

            $result .=
                "], ['unique' => true])\n";
        }

        $result .=
            "->create();";

        return $result;
    }
}