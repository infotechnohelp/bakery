<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Migration;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class LinkingTableTemplate extends Template implements TemplateInterface
{

    public function main()
    {
        $result = '';

        foreach ($this->getInput()->getByKey('linkedTables') as $linkedTable) {

            $columnTitle = Inflector::underscore(Inflector::singularize($linkedTable)) . '_id';

            $result .=
                "->addColumn('$columnTitle', 'integer', [\n" .
                "'default' => null,\n" .
                "'limit' => 11,\n" .
                "'null' => false,\n" .
                "])\n" .
                "->addIndex(['$columnTitle',])\n";

        }


        $result .=
            "->addColumn('created', 'datetime', [\n" .
            "'default' => null,\n" .
            "'limit' => null,\n" .
            "'null' => true,\n" .
            "])\n" .
            "->addIndex(['created',])\n";

        $result .=
            "->addColumn('modified', 'datetime', [\n" .
            "'default' => null,\n" .
            "'limit' => null,\n" .
            "'null' => true,\n" .
            "])\n" .
            "->addIndex(['modified',])\n";


        foreach ($this->getInput()->getByKey('uniqueFieldGroups') as $uniqueGroup) {

            $result .=
                "->addIndex([";

            foreach ($uniqueGroup as $fieldTitle) {
                $result .= "'$fieldTitle', ";
            }

            $result .=
                "], ['unique' => true])\n";

        }

        return $result;
    }
}