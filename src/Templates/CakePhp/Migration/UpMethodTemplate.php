<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Migration;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class UpMethodTemplate extends Template implements TemplateInterface
{
    public function main()
    {
        $result = '';

       foreach($this->getInput()->getByKey('tables') as $table){
           $result .=
               "\$this->$table();\n";
       }

        return $result;
    }
}