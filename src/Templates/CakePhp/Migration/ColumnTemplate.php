<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Migration;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;
use Infotechnohelp\Bakery\Lib\Parser\CakePhpModelSchema;
use Symfony\Component\Yaml\Yaml;

class ColumnTemplate extends Template implements TemplateInterface
{
    private function prepareColumnOptionValue($value)
    {
        if (is_string($value)) {
            return "'$value'";
        }

        if (is_int($value)) {
            return $value;
        }

        if ($value === null) {
            return 'null';
        }

        if ($value === false) {
            return 'false';
        }

        if ($value === true) {
            return 'true';
        }

    }

    public function main()
    {
        $columnTitle = $this->getInput()->getByKey('columnTitle');

        $type = $this->getInput()->getByKey('type');

        $options = $this->getInput()->getByKey('options');

        $result =
            "->addColumn('$columnTitle', '$type', [\n";

        $columnOptions = CakePhpModelSchema::prepareColumnOptions($type, $options);

        $unique = false;

        if ($columnOptions !== null) {
            foreach ($columnOptions as $key => $value) {

                if ($key === 'unique') {
                    $unique = true;
                    continue;
                }

                $value = $this->prepareColumnOptionValue($value);
                $result .= "'$key' => $value,\n";
            }
        }

        $result .= "])\n";

        if ($type !== 'text') {
            $result .=
                "->addIndex(['$columnTitle',]";

            if($unique){
                $result .= ", ['unique' => true]";
            }

            $result .= ")\n";
        }


        return $result;
    }
}