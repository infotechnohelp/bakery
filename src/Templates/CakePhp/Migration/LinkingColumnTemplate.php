<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Migration;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class LinkingColumnTemplate extends Template implements TemplateInterface
{

    public function main()
    {
        $columnTitle = $this->getInput()->getByKey('columnTitle');

        $options = $this->getInput()->getByKey('options');
        
        $null = (array_key_exists('nullable', $options)) ? $options['nullable'] : false;

        $null = $null ? 'true' : 'false';
        
        $result =
            "->addColumn('{$this->getInput()->getByKey('columnTitle')}', 'integer', [\n" .
            "'default' => null,\n" .
            "'limit' => 11,\n" .
            "'null' => $null,\n" .
            "])\n" .
            "->addIndex(['{$this->getInput()->getByKey('columnTitle')}',])\n";

        return $result;
    }
}