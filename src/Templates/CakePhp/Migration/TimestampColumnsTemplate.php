<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Migration;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class TimestampColumnsTemplate extends Template implements TemplateInterface
{
    public function main()
    {
        $result =
            "->addColumn('created', 'datetime', [\n" .
            "'default' => null,\n" .
            "'limit' => null,\n" .
            "'null' => true,\n" .
            "])\n" .
            "->addIndex(['created',])\n";

        $result .=
            "->addColumn('modified', 'datetime', [\n" .
            "'default' => null,\n" .
            "'limit' => null,\n" .
            "'null' => true,\n" .
            "])\n" .
            "->addIndex(['modified',])\n";

        return $result;
    }
}