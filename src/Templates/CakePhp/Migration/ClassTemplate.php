<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Migration;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class ClassTemplate extends Template implements TemplateInterface
{

    public function __construct(string $title = null)
    {
        parent::__construct($title);

        $this->getReservedNeedles()->setByKey("use", "use Migrations\\AbstractMigration;\n");
    }

    public function main()
    {
        // @todo Create a separate method to get tag label
        $firstBakeryTag = array_keys($this->getBakeryItemTags()->getFirstTag()->getTagArray())[0];

        $result =
            "/**\n" .
            " * Class $firstBakeryTag\n";

        if (!empty($this->getGlobalInput()->getByKey('namespace'))) {
            $result .=
                " * @package {$this->getGlobalInput()->getByKey('namespace')}\n";
        }

        $result .=
            "*/\n" .
            "class $firstBakeryTag extends AbstractMigration{\n" .
            "\n" .
            "{$this->needle}\n" .
            "\n" .
            "}";

        return $result;
    }
}