<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Migration;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class DownMethodTemplate extends Template implements TemplateInterface
{
    public function main()
    {
        $result = '';

        foreach($this->getInput()->getByKey('tables') as $table){

            $underscored = Inflector::underscore($table);

            $result .=
                "\$this->table('$underscored')->drop()->update();\n";
        }

        return $result;
    }
}