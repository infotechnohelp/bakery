<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Table;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryConfig;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;
use Infotechnohelp\Bakery\Templates\FileSpecificTemplate;

/**
 * Class ValidationDefaultMethodValidationsTemplate
 * @package Infotechnohelp\Bakery\Templates\CakePhp\Table
 */
class ValidationDefaultMethodValidationsTemplate extends Template implements TemplateInterface
{
    public function __construct(string $title = null, array $input = [], array $options = [], string $configClass = BakeryConfig::class)
    {
        parent::__construct($title, $input, $options, $configClass);
        
        $result = "";

        if (!empty($input)) {
            foreach ($input as $columnTitle => $options) {
                foreach ($options as $optionTitle => $optionConfig) {

                    if ($optionTitle === 'IdsExistIn') {
                        $result .= "use Infotechnohelp\Orm\ValidationRule\IdsExistIn; \n";
                    }
                }
            }
        }

        $this->getReservedNeedles()->setByKey("use", $result);

    }
    
    public function main()
    {
        $input = $this->getInput()->getAll();


        $result = "";

        foreach ($input as $columnTitle => $options) {

            $result .=
                "\$validator";

            foreach ($options as $optionTitle => $optionConfig) {

                if ($optionTitle === 'boolean') {
                    $result .=
                        "\n->boolean('$columnTitle')";
                }

                if ($optionTitle === 'scalar') {
                    $result .=
                        "\n->scalar('$columnTitle')";
                }

                if ($optionTitle === 'dateTime') {
                    $result .=
                        "\n->dateTime('$columnTitle')";
                }

                if ($optionTitle === 'numeric') {
                    $result .=
                        "\n->numeric('$columnTitle')";
                }

                if ($optionTitle === 'integer') {
                    $result .=
                        "\n->integer('$columnTitle')";
                }

                if ($optionTitle === 'greaterThanOrEqual') {
                    $result .=
                        "\n->greaterThanOrEqual('$columnTitle', $optionConfig[0])";
                }

                if ($optionTitle === 'maxLength') {
                    $result .=
                        "\n->maxLength('$columnTitle', $optionConfig[0])";
                }

                if ($optionTitle === 'minLength') {
                    $result .=
                        "\n->minLength('$columnTitle', $optionConfig[0])";
                }

                if ($optionTitle === 'isJson') {
                    $result .=
                        "\n->add('$columnTitle', 'isJson', [" .
                        "\n'rule' => function (\$value) {" .
                        "\njson_decode(\$value);" .
                        "\nreturn (json_last_error() === JSON_ERROR_NONE) ? true : false;" .
                        "\n}," .
                        "\n'message' => \"The field '$columnTitle' should be json\"," .
                        "\n])";

                }

                if ($optionTitle === 'isUnique') {
                    $result .= "\n->add('$columnTitle', 'unique', ['rule' => 'validateUnique', 'provider' => 'table'])";
                }

                if ($optionTitle === 'requirePresence') {

                    $result .=
                        "\n->requirePresence('$columnTitle'";

                    if ($optionConfig[0] !== null) {
                        $result .= ", '{$optionConfig[0]}'";
                    }

                    $result .= ")";

                }

                if ($optionTitle === 'allowEmptyString') {

                    $result .=
                        "\n->allowEmptyString('$columnTitle'";

                    if ($optionConfig[0] !== null) {
                        if ($optionConfig[0] !== false) {
                            $result .= ", '{$optionConfig[0]}'";
                        } else {
                            $result .= ", false";
                        }
                    }

                    $result .= ")";
                }


                if ($optionTitle === 'IdsExistIn') {

                    $tableAlias = Inflector::camelize($columnTitle);

                    $result .=
                        "\n->add('$columnTitle', 'IdsExistIn', [" .
                        "\n'rule' => (new IdsExistIn('$tableAlias'))," .
                        "\n'message' => \"Some provided $tableAlias ids do not exist. Take a look at error Logs\"," .
                        "\n])";

                }

            }

            $result .=
                ";\n" .
                "\n";

        }

        $result .= (new FileSpecificTemplate(null, ['src/Model/Table', 'defaultValidations']))
            ->setBakeryItemTags($this->getBakeryItemTags())
            ->getMarkedResult();


        return $result;
    }
}