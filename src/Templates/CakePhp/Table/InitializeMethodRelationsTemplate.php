<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Table;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

/**
 * Class InitializeMethodRelationsTemplate
 * @package Infotechnohelp\Bakery\Templates\CakePhp\Table
 */
class InitializeMethodRelationsTemplate extends Template implements TemplateInterface
{
    public function main()
    {
        $input = $this->getInput()->getAll();

        $result = "";

        foreach ($input as $relationTitle => $tables) {

            foreach ($tables as $tableTitle => $tableConfig) {

                $pluginPrefix = $tableConfig['pluginPrefix'] ?? null;

                if ($pluginPrefix !== null) {
                    $pluginPrefix .= '.';
                    $fullTableTitle = "$pluginPrefix$tableTitle";
                }

                $singular = Inflector::underscore(Inflector::singularize($tableTitle));

                $foreignKey = (array_key_exists('customLinkingColumn', $tableConfig)) ?
                    $tableConfig['customLinkingColumn'] :
                    "{$singular}_id";

                if ($relationTitle === 'belongsTo') {

                    $result .=
                        "\$this->belongsTo('$tableTitle', [\n";

                    if(isset($fullTableTitle)){
                        $result .=
                        "'className' => '$fullTableTitle',\n";
                    }

                    $result .=
                        "'foreignKey' => '$foreignKey',\n";

                    if(array_key_exists('propertyName', $tableConfig)) {
                        $result .=
                            "'propertyName' => '{$tableConfig['propertyName']}',\n";
                    }
                        
                    
                    $result .=
                        "'joinType' => 'LEFT',\n" .
                        "]);\n" .
                        "\n";
                }


                if ($relationTitle === 'belongsToMany') {

                    $globalTableTitle = array_keys($this->getBakeryItemTags()->getFirstTag()->getTagArray())[0];

                    $singularGlobal = Inflector::underscore(Inflector::singularize($globalTableTitle));

                    $tableTitles = [$tableTitle, $globalTableTitle];
                    sort($tableTitles);

                    $linkingTable = Inflector::underscore(implode('', $tableTitles));

                    $result .=
                        "\$this->belongsToMany('$tableTitle', [\n";

                    if(isset($fullTableTitle)){
                        $result .=
                            "'className' => '$fullTableTitle',\n";
                    }

                    $result .=
                        "'foreignKey' => '{$singularGlobal}_id',\n" .
                        "'targetForeignKey' => '{$singular}_id',\n" .
                        "'joinTable' => '$linkingTable',\n" .
                        "]);\n" .
                        "\n";
                }


                if (in_array($relationTitle, ['hasMany', 'hasOne'], true)) {

                    $globalTableTitle = array_keys($this->getBakeryItemTags()->getFirstTag()->getTagArray())[0];

                    $singularGlobal = Inflector::underscore(Inflector::singularize($globalTableTitle));

                    $foreignKey = (array_key_exists('foreignKey', $tableConfig)) ?
                        $tableConfig['foreignKey'] :
                        $singularGlobal . "_id";

                    $result .=
                        "\$this->$relationTitle('$tableTitle', [\n";

                    if(isset($fullTableTitle)){
                        $result .=
                            "'className' => '$fullTableTitle',\n";
                    }

                    $result .=
                        "'foreignKey' => '$foreignKey',\n" .
                        "]);\n" .
                        "\n";
                }

                unset($fullTableTitle);
            }
        }

        return $result;
    }
}