<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Table;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\BakeryConfig;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;
use Infotechnohelp\Bakery\Templates\FileSpecificTemplate;

/**
 * Class BuildRulesMethodBodyTemplate
 * @package Infotechnohelp\Bakery\Templates\CakePhp\Table
 */
class BuildRulesMethodBodyTemplate extends Template implements TemplateInterface
{

    public function __construct(string $title = null, array $input = [], array $options = [], string $configClass = BakeryConfig::class)
    {
        parent::__construct($title, $input, $options, $configClass);

        list($input, $additional) = $this->getInput()->getAll();

        $result = "";

        if (!empty($input)) {

            if (array_key_exists('requireAtLeastOneOf', $input)) {
                $result .= "use Infotechnohelp\Orm\Rule\RequireAtLeastOneOf;\n";
            }

            if (array_key_exists('requireAtLeastOneOfGroups', $input)) {
                $result .= "use Infotechnohelp\Orm\Rule\RequireAtLeastOneOfGroups;\n";
            }

            if (array_key_exists('requireOnlyOneOf', $input)) {
                $result .= "use Infotechnohelp\Orm\Rule\RequireOnlyOneOf;\n";
            }

            if (array_key_exists('requireOnlyOneOfGroups', $input)) {
                $result .= "use Infotechnohelp\Orm\Rule\RequireOnlyOneOfGroups;\n";
            }

            if (array_key_exists('NotEmpty', $input)) {
                $result .= "use Infotechnohelp\Orm\Rule\NotEmpty;\n";
            }
        }

        $this->getReservedNeedles()->setByKey("use", $result);
    }


    private function prepareStrings(array $columns, bool $groups = false)
    {
        if (!$groups) {
            $resultString = "";

            $i2 = 0;
            foreach ($columns as $column) {

                if ($i2 === count($columns) - 1) {
                    $resultString .= "'$column'";
                    break;
                }

                $resultString .= "'$column', ";

                $i2++;
            }

            $result[] = $resultString;

            $resultString = "";

            $i2 = 0;
            foreach ($columns as $column) {

                if ($i2 === count($columns) - 1) {
                    $resultString .= "$column";
                    break;
                }

                $resultString .= "$column || ";

                $i2++;
            }

            $result[] = $resultString;

            return $result;
        }


        $fullResultString = "";

        $i1 = 0;

        foreach ($columns as $columnGroup) {

            $resultString = "[";
            $i2 = 0;

            foreach ($columnGroup as $column) {

                if ($i2 === count($columnGroup) - 1) {
                    $resultString .= "'$column']";
                    break;
                }

                $resultString .= "'$column', ";

                $i2++;
            }

            if ($i1 === count($columns) - 1) {
                $fullResultString .= $resultString;
                break;
            }

            $fullResultString .= $resultString . ", ";

            $i1++;
        }

        $result[] = $fullResultString;


        $fullResultString = "";

        $i1 = 0;

        foreach ($columns as $columnGroup) {

            $resultString = "";
            $i2 = 0;

            foreach ($columnGroup as $column) {

                if ($i2 === count($columnGroup) - 1) {
                    $resultString .= "$column";
                    break;
                }

                $resultString .= "$column & ";

                $i2++;
            }

            if ($i1 === count($columns) - 1) {
                $fullResultString .= $resultString;
                break;
            }

            $fullResultString .= $resultString . " || ";

            $i1++;
        }

        $result[] = $fullResultString;


        return $result;
    }


    public function main()
    {
        list($input, $additional) = $this->getInput()->getAll();

        $input = (empty($input)) ? [] : $input;

        $additional = (empty($additional)) ? [] : $additional;

        $result = "";

        if (!empty($additional)) {

            if (array_key_exists('uniqueFieldGroups', $additional)) {


                foreach ($additional['uniqueFieldGroups'] as $group) {

                    if(count($group) === 1){
                        $result .= "\$rules->add(\$rules->isUnique(['{$group[0]}']));\n\n";
                        continue;
                    }

                    $result .=
                        "\$rules->add(\$rules->isUnique(\n" .
                    "[";

                    foreach ($group as $fieldTitle) {
                        $result .= "'$fieldTitle', ";
                    }

                    $result .=
                        "],\n" .
                    "\"This [";

                    foreach ($group as $fieldTitle) {
                        $result .= "'$fieldTitle', ";
                    }

                    $result .=
                        "] combination has already been used.\"\n" .
                        "));\n\n";
                }
            }
        }

        foreach ($input as $ruleTitle => $config) {

            if ($ruleTitle === 'existsIn') {

                foreach ($config as $tableTitle => $tableConfig) {

                    // CakePHP checks against nullables, can be present
                    /*
                    if(array_key_exists('nullable', $tableConfig) && $tableConfig['nullable']){
                        continue;
                    }
                    */

                    $underscoredTitle = Inflector::underscore(Inflector::singularize($tableTitle));


                    $columnTitle = (array_key_exists('customLinkingColumn', $tableConfig)) ?
                        $tableConfig['customLinkingColumn'] :
                        "{$underscoredTitle}_id";

                    $result .= "\$rules->add(\$rules->existsIn(['$columnTitle'], '$tableTitle'));\n\n";
                }

            }


            if ($ruleTitle === 'isUnique') {

                foreach ($config as $columnTitle) {
                    $result .= "\$rules->add(\$rules->isUnique(['$columnTitle']));\n\n";
                }

            }

            if ($ruleTitle === 'requireAtLeastOneOf') {

                foreach ($config as $columns) {
                    $preparedColumnsStrings = $this->prepareStrings($columns);

                    $result .=
                        "\$rules->add(new RequireAtLeastOneOf([{$preparedColumnsStrings[0]}]), 'requireAtLeastOneOf', [\n" .
                        "'errorField' => '{$preparedColumnsStrings[1]}',\n" .
                        "'message' => 'At least one field is required'\n" .
                        "]);\n" .
                        "\n";

                }

            }

            if ($ruleTitle === 'requireOnlyOneOf') {

                foreach ($config as $columns) {
                    $preparedColumnsStrings = $this->prepareStrings($columns);

                    $result .=
                        "\$rules->add(new RequireOnlyOneOf([{$preparedColumnsStrings[0]}]), 'requireOnlyOneOf', [\n" .
                        "'errorField' => '{$preparedColumnsStrings[1]}',\n" .
                        "'message' => 'Only one field is required'\n" .
                        "]);\n" .
                        "\n";

                }

            }

            if ($ruleTitle === 'requireAtLeastOneOfGroups') {

                foreach ($config as $columns) {
                    $preparedColumnsStrings = $this->prepareStrings($columns, true);


                    $result .=
                        "\$rules->add(new RequireAtLeastOneOfGroups([{$preparedColumnsStrings[0]}]), 'requireAtLeastOneOfGroups', [\n" .
                        "'errorField' => '{$preparedColumnsStrings[1]}',\n" .
                        "'message' => 'At least one field group is required'\n" .
                        "]);\n" .
                        "\n";

                }

            }


            if ($ruleTitle === 'requireOnlyOneOfGroups') {

                foreach ($config as $columns) {
                    $preparedColumnsStrings = $this->prepareStrings($columns, true);

                    $result .=
                        "\$rules->add(new RequireOnlyOneOfGroups([{$preparedColumnsStrings[0]}]), 'requireOnlyOneOfGroups', [\n" .
                        "'errorField' => '{$preparedColumnsStrings[1]}',\n" .
                        "'message' => 'Only one field group is required'\n" .
                        "]);\n" .
                        "\n";

                }

            }

            if ($ruleTitle === 'NotEmpty') {

                foreach ($config as $columnTitle) {
                    $result .=
                        "\$rules->add(new NotEmpty('$columnTitle'), 'NotEmpty', [\n" .
                        "'errorField' => '$columnTitle',\n" .
                        "'message' => \"Field '$columnTitle' cannot be left empty\",\n" .
                        "]);\n" .
                        "\n";
                }

            }


        }


        $result .= (new FileSpecificTemplate(null, ['src/Model/Table', 'buildRules']))
            ->setBakeryItemTags($this->getBakeryItemTags())
            ->getMarkedResult();

        $result .= "\nreturn \$rules;\n";

        return $result;
    }
}