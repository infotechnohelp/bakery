<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Table;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

/**
 * Class InitializeMethodBodyTemplate
 * @package Infotechnohelp\Bakery\Templates\CakePhp\Table
 */
class InitializeMethodBodyTemplate extends Template implements TemplateInterface
{
    public function main()
    {
        $tableTitle = array_keys($this->getBakeryItemTags()->getFirstTag()->getTagArray())[0];

        $underscoredTableTitle = Inflector::underscore($tableTitle);

        $timestampBehaviour = $this->getGlobalInput()->getByKey('timestampBehaviour');

        $result =
            "parent::initialize(\$config);\n" .
            "\n" .
            "\$this->setTable('$underscoredTableTitle');\n" .
            // "\$this->setDisplayField('id');\n" .
            "\$this->setPrimaryKey('id');\n" .
            "\n";

        if ($timestampBehaviour) {
            $result .=
                "\$this->addBehavior('Timestamp');\n" .
                "\n";
        }

        $result .= "{$this->getNeedle()}\n";

        return $result;
    }
}