<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Table;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

/**
 * Class InitializeMethodAddRelationsTemplate
 * @package Infotechnohelp\Bakery\Templates\CakePhp\Table
 */
class InitializeMethodAddRelationsTemplate extends Template implements TemplateInterface
{
    public function main()
    {
        $input = $this->getInput()->getAll();

        $result = "";

        foreach ($input as $relationTitle => $config) {

            $title = array_keys($config)[0];

            $result .=
                "\$this->$relationTitle('$title', [\n";

            $config = array_values($config)[0];

            foreach ($config as $key => $value) {
                if($key === 'conditions'){
                    $result .= "'$key' => $value,\n";
                    continue;
                }

                $result .= "'$key' => '$value',\n";
            }

            $result .= "]);\n\n";
        }

        return $result;
    }
}