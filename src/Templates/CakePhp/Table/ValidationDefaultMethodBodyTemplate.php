<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Table;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

/**
 * Class ValidationDefaultMethodBodyTemplate
 * @package Infotechnohelp\Bakery\Templates\CakePhp\Table
 */
class ValidationDefaultMethodBodyTemplate extends Template implements TemplateInterface
{
    public function main()
    {
        $result =
            "\$validator\n" .
            "->integer('id')\n" .
            "->allowEmptyString('id', 'create');\n" .
            "\n" .
            "{$this->getNeedle()}\n" .
            "\n" .
            "return \$validator;";

        return $result;
    }
}