<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Entity;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class EntityClassTemplate extends Template implements TemplateInterface
{
    public function __construct(string $title = null)
    {
        parent::__construct($title);

        $result =
            "use Cake\\ORM\\Entity;\n";

        $this->getReservedNeedles()->setByKey("use", $result);
    }

    public function main()
    {
        // @todo Create a separate method to get tag label
        $firstBakeryTag = array_keys($this->getBakeryItemTags()->getFirstTag()->getTagArray())[0];

        $result =
            "/**\n" .
            " * Class {$firstBakeryTag}\n";

        if (!empty($this->getGlobalInput()->getByKey('namespace'))) {
            $result .=
                " * @package {$this->getGlobalInput()->getByKey('namespace')}\n";
        }

        $result .=
            "*/\n" .
            "class {$firstBakeryTag} extends Entity{\n" .
            "\n" .
            "{$this->needle}\n" .
            "\n" .
            "}";

        return $result;
    }
}
