<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Entity;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;
use Infotechnohelp\Bakery\Templates\FileSpecificTemplate;

/**
 * Class InitializeMethodBodyTemplate
 * @package Infotechnohelp\Bakery\Templates\CakePhp\Entity
 */
class EntityClassBodyTemplate extends Template implements TemplateInterface
{
    public function main()
    {
        $timestampBehaviour = $this->getInput()->getByKey('timestampBehaviour');
        $columns = $this->getInput()->getByKey('columns');

        $result =
            "/**\n" .
            " * @var array\n" .
            " */\n" .
            "protected \$_accessible = [\n";

        foreach ($columns as $column){
            $result .= "'$column' => true,\n";
        }

        if($timestampBehaviour){
            $result .=
                "'created' => true,\n" .
                "'modified' => true,\n";
        }

        $result .= (new FileSpecificTemplate(null, ['src/Model/Entity', 'accessible']))
            ->setBakeryItemTags($this->getBakeryItemTags())
            ->getMarkedResult();

        $result .= "];\n";

        return $result;
    }

}