<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

/**
 * Class EntityVirtualFieldsTemplate
 * @package Infotechnohelp\Bakery\Templates\CakePhp\Entity
 */
class EntityVirtualFieldsTemplate extends Template implements TemplateInterface
{
    public function main()
    {
        $input = $this->getInput()->getAll();

        $result = "";

        if (!empty($input)) {

            $result .= "protected \$_virtual = [\n";

            foreach ($input as $virtualFieldTitle => $pathTitles) {
                $result .= "'$virtualFieldTitle',\n";
            }

            $result .= "];\n\n";


            foreach ($input as $virtualFieldTitle => $pathTitles) {

                $camelized = Inflector::camelize($virtualFieldTitle);

                $result .= "protected function _get$camelized() {\n";

                $i = 1;
                $entityTitle = null;

                foreach ($pathTitles as $pathTitle) {

                    if ($i === count($pathTitles)) {

                        $result .= "return (!empty($entityTitle)) ? {$entityTitle}->get('$pathTitle') : false;\n";

                        break;
                    }

                    $camelized = Inflector::camelize($pathTitle);

                    $entityTitle = "\$$camelized";

                    $result .=
                        "/** @var Entity $entityTitle */\n" .
                        "$entityTitle = \$this->_properties['$pathTitle'];\n";


                    $i++;
                }


                $result .= "}\n\n";
            }
        }

        return $result;
    }

}