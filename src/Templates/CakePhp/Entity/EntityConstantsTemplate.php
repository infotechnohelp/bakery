<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Entity;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

/**
 * Class EntityConstantsTemplate
 * @package Infotechnohelp\Bakery\Templates\CakePhp\Entity
 */
class EntityConstantsTemplate extends Template implements TemplateInterface
{
    public function main()
    {
        $data = $this->getInput()->getAll();

        $result = "";

        if (!empty($data) && array_key_exists('constantContainer', $data)) {

            foreach ($data['seeds'] as $index => $value) {

                if(!is_array($value)){
                    $constant = strtoupper(str_replace([' ', '-', '/'], '_', $value));
                    $result .= "const " . $constant . " = " . ($index + 1) . ";\n";
                    continue;
                }

                $constant = strtoupper(str_replace([' ', '-', '/'], '_', $value[$data['constantField']]));
                $result .= "const " . $constant . " = " . ($index + 1) . ";\n";
            }

            $result .= "\n";
        }

        return $result;
    }

}