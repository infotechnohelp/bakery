<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class ShellClassTemplate extends Template implements TemplateInterface
{

    public function __construct(string $title = null)
    {
        parent::__construct($title);

        $this->getReservedNeedles()->setByKey("use", "use Cake\\Console\\Shell;\n");
    }

    public function main()
    {
        // @todo Create a separate method to get tag label
        $firstBakeryTag = array_keys($this->getBakeryItemTags()->getFirstTag()->getTagArray())[0];

        $result =
            "/**\n" .
            " * Class $firstBakeryTag\n";

        if (!empty($this->getGlobalInput()->getByKey('namespace'))) {
            $result .=
                " * @package {$this->getGlobalInput()->getByKey('namespace')}\n";
        }

        $result .=
            "*/\n" .
            "class $firstBakeryTag extends Shell{\n" .
            "\n" .
            "{$this->needle}\n" .
            "\n" .
            "}";

        return $result;
    }
}