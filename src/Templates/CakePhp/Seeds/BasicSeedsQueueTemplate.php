<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Seeds;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;
use Infotechnohelp\Bakery\Templates\FileSpecificTemplate;

class BasicSeedsQueueTemplate extends Template implements TemplateInterface
{

    public function main()
    {
        $result = "return  [\n";

        $result .= (new FileSpecificTemplate(null, ['config/TrackedSeeds', 'start']))
            ->setBakeryItemTags($this->getBakeryItemTags())
            ->getMarkedResult();
        
        $input = $this->getInput()->getAll();

        foreach ($input as $index => $value) {
            if (array_key_exists('data', $value)) {

                $tableTitle = Inflector::pluralize($value['fileTitle']);

                $result .= "'$tableTitle',\n";
            }
        }

        $result .= (new FileSpecificTemplate(null, ['config/TrackedSeeds', 'end']))
            ->setBakeryItemTags($this->getBakeryItemTags())
            ->getMarkedResult();

        return $result . "];";
    }
}
