<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\Seeds;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class BasicSeedsTemplate extends Template implements TemplateInterface
{

    public function main()
    {
        $fileTitle = $this->getInput()->getByKey('fileTitle');

        $seeds = $this->getInput()->getByKey('data')['seeds'];


        $result = "\$tableAlias = \"" . Inflector::pluralize($fileTitle) . "\";\n\n";

        $result .= "\$data = [\n";

        foreach ($seeds as $index => $value) {

            $result .= "[\n";

            if (is_array($value)) {
                foreach ($value as $key => $value2){
                    $result .= "'$key' => '$value2',\n";
                }

                $result .= "],\n";

                continue;
            }

            $fieldTitle = $this->getInput()->getByKey('data')['seedField'];

            $result .= "'$fieldTitle' => '$value',\n";

            $result .= "],\n";
        }

        $result .= "];";

        /*
        $tableAlias = "AuthApi.UserRoles";

        $data = [
            [
                'title' => 'User',
            ],
            [
                'title' => 'Admin',
            ],
            [
                'title' => 'Moderator',
            ],
        ];
*/
        return $result;
    }
}
