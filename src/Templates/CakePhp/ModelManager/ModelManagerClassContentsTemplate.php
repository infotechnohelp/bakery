<?php declare(strict_types=1);

namespace Infotechnohelp\Bakery\Templates\CakePhp\ModelManager;

use Cake\Utility\Inflector;
use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;
use Infotechnohelp\Bakery\Templates\FileSpecificTemplate;

class ModelManagerClassContentsTemplate extends Template implements TemplateInterface
{

    public function __construct(string $title = null)
    {
        parent::__construct($title);
    }

    public function main()
    {
        // @todo Create a separate method to get tag label
        $firstBakeryTag = array_keys($this->getBakeryItemTags()->getFirstTag()->getTagArray())[0];

        $requiredLinkingFields = $this->getInput()->getByKey('insert')['requiredLinkingFields'] ?? null;

        $requiredFields = $this->getInput()->getByKey('insert')['requiredFields'] ?? null;


        $result =
            "/**\n" .
            " * @var string\n" .
            " */\n" .
            "protected \$tableAlias = '{$firstBakeryTag}';\n" .
            "\n" .
            "protected \$containAllAliases = [\n" .
            (new FileSpecificTemplate(null, ['src/ModelManager', 'containAllAliases']))
                ->setBakeryItemTags($this->getBakeryItemTags())
                ->getMarkedResult() .
            "];\n" .
            "\n" .
            "/**\n";

        if ($requiredLinkingFields !== null) {
            foreach ($requiredLinkingFields as $requiredLinkingField) {
                list($foreignKey, $propertyName) = $requiredLinkingField;

                // lcfirst = firstLetterLowerCase
                $camelizedForeignKey = lcfirst(Inflector::camelize($foreignKey));

                $result .= " * @param $$camelizedForeignKey\n";

            }
        }

        if ($requiredFields !== null) {
            foreach ($requiredFields as $requiredField) {
                list($type, $foreignKey) = $requiredField;

                // lcfirst = firstLetterLowerCase
                $camelizedForeignKey = lcfirst(Inflector::camelize($foreignKey));

                $result .= " * @param $type $$camelizedForeignKey\n";
            }
        }


        $result .=
            " * @param array \$_data\n" .
            " * @return \Cake\Datasource\EntityInterface\n" .
            " */\n" .
            "public function insert(\n";


        if ($requiredLinkingFields !== null) {
            foreach ($requiredLinkingFields as $requiredLinkingField) {
                list($foreignKey, $propertyName) = $requiredLinkingField;

                // lcfirst = firstLetterLowerCase
                $camelizedForeignKey = lcfirst(Inflector::camelize($foreignKey));

                $result .= " $$camelizedForeignKey,\n";

            }
        }

        if ($requiredFields !== null) {
            foreach ($requiredFields as $requiredField) {
                list($type, $foreignKey) = $requiredField;

                // lcfirst = firstLetterLowerCase
                $camelizedForeignKey = lcfirst(Inflector::camelize($foreignKey));

                $result .= "$type $$camelizedForeignKey,\n";
            }
        }

        $result .=
            "array \$_data = []\n" .
            ")\n" .
            "{\n";


        if (!empty($requiredLinkingFields)) {
            $result .=
                "\$dynamicData = [];\n" .
                "\n";
        }

        if ($requiredLinkingFields !== null) {
            foreach ($requiredLinkingFields as $requiredLinkingField) {
                list($foreignKey, $propertyName) = $requiredLinkingField;

                // lcfirst = firstLetterLowerCase
                $camelizedForeignKey = lcfirst(Inflector::camelize($foreignKey));

                $result .=
                    "if (is_array(\$$camelizedForeignKey)) {\n" .
                    "\$dynamicData['$propertyName'] = \$$camelizedForeignKey;\n" .
                    "} else {\n" .
                    "\$dynamicData['$foreignKey'] = \$$camelizedForeignKey;\n" .
                    "}\n" .
                    "\n";

            }
        }

        $result .=
            "\$Entity = \$this->table->newEntity(\n" .

            "array_merge(\n" .
            "[\n";

        if ($requiredFields !== null) {
            foreach ($requiredFields as $requiredField) {
                list($type, $foreignKey) = $requiredField;

                // lcfirst = firstLetterLowerCase
                $camelizedForeignKey = lcfirst(Inflector::camelize($foreignKey));

                $result .= "'$foreignKey' => $$camelizedForeignKey,\n";
            }
        }

        $result .=
            "],\n";

        if (!empty($requiredLinkingFields)) {
            $result .=
                "\$dynamicData,\n";
        }

        $result .= 
            "\$_data\n" .
            ")\n" .
            ");\n" .
            "\n" .
            "return \$this->table->saveOrFail(\$Entity);\n" .
            "}\n";

        return $result;
    }
}