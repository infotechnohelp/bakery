<?php declare(strict_types = 1);

namespace Infotechnohelp\Bakery\Templates\Test;

use Infotechnohelp\Bakery\Lib\Bakery\Template;
use Infotechnohelp\Bakery\Lib\Bakery\TemplateInterface;

class TestTemplate extends Template implements TemplateInterface
{
    public function __construct(string $title = null)
    {
        parent::__construct($title);

        $this->getReservedNeedles()->setByKey("use", "use Cake\\Console\\Shell2;\n");
    }


    public function main()
    {
        $result =
            "Test me";

        return $result;
    }
}